<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::namespace('Admin')->group(function () {
        Route::group([], function () {
            Route::get('booking/sendhotel/{id}', 'BookingController@sendhotel');
            Route::get('booking/createwhithotel/{hotel}', 'BookingController@createwhithotel');
            Route::get('booking/cancelledreservation/{hotel}', 'BookingController@cancelledreservation');
            Route::get('guest/sendhotel/{id}', 'GuestController@sendhotel');
            Route::get('guest/createwhithotel/{hotel}', 'GuestController@createwhithotel');
            Route::get('categorie/sendhotel/{id}', 'CategorieController@sendhotel');
            Route::get('roomtype/sendhotel/{id}', 'RoomTypeController@sendhotel');
            Route::get('roomtype/createwhithotel/{hotel}', 'RoomTypeController@createwhithotel');
            Route::post('roomtype/createcategoria', 'CategorieController@createcategoria')->name('createcategoria');
            Route::get('product/sendhotel/{id}', 'ProductController@sendhotel');
            Route::get('product/createwhithotel/{hotel}', 'ProductController@createwhithotel');
            Route::get('consumeproduct/sendhotel/{id}', 'ConsumeProductController@sendhotel');
            Route::get('consumeproduct/createwhithotel/{hotel}', 'ConsumeProductController@createwhithotel');
            Route::post('hotel/deleteimglogo', 'HotelController@deleteImgLogo')->name('deleteimglogo');
            Route::post('hotel/deleteImg', 'HotelController@deleteImg')->name('deleteImg');
            Route::post('product/deleteimg', 'ProductController@deleteImg')->name('deleteimg');
            Route::post('booking/roomPrice', 'BookingController@roomPrice')->name('roomPrice');
            Route::post('booking/consultDocument', 'BookingController@consultDocument')->name('consultDocument');
            Route::middleware('auth')->post('/admin/booking/buscarRooms', 'Admin\BookingController@buscarRooms')->name('buscarRooms');
            Route::post('consumeproduct/productPrice', 'ConsumeProductController@productPrice')->name('productPrice');
            Route::post('consumeproduct/productPrice_', 'ConsumeProductController@productPrice_')->name('productPrice_');
            Route::post('activity/deleteImg', 'ActivityController@deleteImg')->name('deleteImg');
            Route::post('booking/saveReason', 'BookingController@saveReason')->name('saveReason');
            Route::post('booking/deleteBooking', 'BookingController@deleteBooking')->name('deleteBooking');
            Route::post('hotel/suspenderHotel', 'HotelController@suspenderHotel')->name('suspenderHotel');
            Route::post('hotel/deleteHotel', 'HotelController@deleteHotel')->name('deleteHotel');
            Route::post('activity/deleteActivity', 'ActivityController@deleteActivity')->name('deleteActivity');
            Route::post('roomtype/deleteRoomType', 'RoomTypeController@deleteRoomType')->name('deleteRoomType');
            Route::post('product/deleteProduct', 'ProductController@deleteProduct')->name('deleteProduct');
            Route::post('categorie/deleteCategorie', 'CategorieController@deleteCategorie')->name('deleteCategorie');
            Route::post('hotel/fetch', 'HotelController@fetch')->name('fetch');
            Route::post('hotel/searchEmailUser', 'HotelController@searchEmailUser')->name('searchEmailUser');
            Route::get('booking/statistics', 'BookingController@statistics');
            Route::resources([
                'hotel'          => 'HotelController',
                'activity'       => 'ActivityController',
                'booking'        => 'BookingController',
                'guest'          => 'GuestController',
                'categorie'      => 'CategorieController',
                'roomtype'       => 'RoomTypeController',
                'product'        => 'ProductController',
                'consumeproduct' => 'ConsumeProductController',
                //'/usuarios/user'     => 'UsersController'
            ]);
        });
    });
});



Route::group(['middleware' => ['auth']], function () {
    Route::resource('usuarios', 'UsersController');
});
