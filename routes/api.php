<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/registerBooking', 'Api\BookingController@storeBooking');
Route::post('/registerCheckin', 'Api\BookingController@registerCheckin');

Route::get('/logout', 'Api\UsersController@logout')->middleware('auth:api');

Route::middleware('cors', 'auth:api')->get('/auth', 'Api\UsersController@index');
Route::group(['middleware' => ['cors']], function () {
    Route::namespace('Api')->group(function () {
        Route::group([], function () {
            Route::get('/activities', 'ActivitiesController@index');
            Route::post('/reservas/comprobar', 'BookingController@verifyCode');
            Route::post('/reservas/checkin', 'BookingController@changeCheckin');
            Route::post('/reservas/consultaid', 'BookingController@verifyId');
            Route::post('/reservas/checkout', 'BookingController@checkout');
            Route::post('/hotel/consultar', 'HotelController@searchHotel');
            Route::post('/servicios/consultar', 'ProductController@listProducts');
            Route::post('/servicios/categorias', 'ProductController@searchProducts');
            Route::post('/servicios/productos', 'ProductController@categoryProducts');
            Route::post('/consumos/consultar', 'ConsumeProductController@checkConsumption');
            Route::post('/consumos/ingresar', 'ConsumeProductController@createConsumeProduct');
            Route::post('/categorias/consultar', 'CategorieController@listCategories');
        });
    });
});
