<?php
    use App\User;
?>
@extends('layouts.platform')

@section('content')
    <div class="row container_center">
        <div class="col-12 text-center">
            <div class="title_list">
                <h1>Listado de Usuarios</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="row content_center">
                <a href="{{ URL::to('/usuarios/create') }}" class="btn btn-primary">Agregar Usuario</a>
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Nombre</th>
                        <th>Hotel</th>
                        <th>Email</th>
                        <th class="col-xs-2 text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $us)
                        <tr>
                            <td>{{ $us->name }}</td>
                            <td>
                                @foreach($hotel as $h)
                                    @if($us->id_hotel==$h->id)
                                        {{ $h->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{ $us->email }}</td>
                            <td class="text-center">
                                <a class="btn_menu" href="{{ url('/usuarios/'.$us->id.'/edit') }}">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                </a>
                                <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $us->id }}">
                                    <i class="fa fa-trash" title="Eliminar"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
