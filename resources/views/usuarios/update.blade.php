<?php
?>
@extends('layouts.platform')
@section('content')
    <div class="container_center">
        <div class="title_top">
            <a href="{{ URL::to('/usuarios/') }}">
                <i class="fa fa-sign-in"></i>
            </a>
            <h1 class="text-center">Editar Usuario</h1>
        </div>
        {!! Form::model($usuario, [
            'method' => 'PATCH',
            'route' => ['usuarios.update', $usuario->id],
            'enctype'=> 'multipart/from-data',
            'files'=> true,
            'name' => 'form_edit_user'
        ]) !!}
                    
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información del Usuario</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                    		<div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                    <input type="text" name="name" id="name" class="form-control input-lg" value="{{ $usuario->name }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('emailadmin', 'Email:', ['class' => 'control-label']) !!}
                                    <input type="email" class="form-control" id="emailadmin1" name="emailadmin" onBlur="validarEmail('1');" value="{{ $usuario->email }}" required>
                                    <small class="text-danger ocult">El email ya esta registrado en el sistema por favor ingresa otro.</small>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                            <div class="col-12 text-center btn_save">
                                <div class="justify-content-center">
                                    {!! Form::submit('Editar Usuario', ['class' => 'btn btn-primary']) !!}

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function validarEmail(v){
        var query = $('#emailadmin'+v).val();
        var _token = $('input[name="_token"]').val();
        $.ajax({
          url:"{{ route('searchEmailUser') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
            if(data.length > 0){
                $('#emailadmin'+v).val('');
                $('.ocult').css('display','block');
            }else{
                $('.ocult').css('display','none');
            }
          }
        });
    }
    </script>
@endsection