@extends('layouts.platform')

@section('content')
<div class="row justify-content-center px-2">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h2>Dashboard</h2>
            </div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                Bienvenido {{ Auth::user()->name }}
            </div>
        </div>
    </div>
</div>
@endsection
