<?php
    use App\Entities\OccupiedRooms\OccupiedRoom;
    use Illuminate\Support\Facades\DB;
?>
@extends('layouts.platform')

@section('content')
<div class="row container_center">
  <div class="col-12 menu_hotel">
    <div class="row">
      <div class="col-2 text-center">
        <a class="btn_menu" href="#">
          <i class="fa fa-calendar"></i>
        </a>
        <div>Administrar Reservas</div>
      </div>
      <div class="col-2 text-center">
        <a class="btn_menu" href="{{ URL::to('/admin/roomtype/sendhotel/'.$hotel) }}">
          <i class="fa fa-bed"></i>
          <div>Administrar Habitaciones</div>
        </a>
      </div>
      <div class="col-2 text-center">
        <a class="btn_menu" href="{{ URL::to('/admin/guest/sendhotel/'.$hotel) }}">
          <i class="fa fa-users"></i>
          <div>Administrar Huéspedes</div>
        </a>
      </div>
      <div class="col-2 text-center">
        <a class="btn_menu" href="{{ URL::to('/admin/consumeproduct/sendhotel/'.$hotel) }}">
          <i class="fa fa-list-ol"></i>
          <div>Consumos Huéspedes</div>
        </a>
      </div>
      <div class="col-2 text-center">
        <a class="btn_menu" href="{{ URL::to('/admin/product/sendhotel/'.$hotel) }}">
          <i class="fa fa-cutlery"></i>
          <div>Productos y Servicios</div>
        </a>
      </div>
      <div class="col-2 text-center">
        <a class="btn_menu" href="{{ URL::to('/admin/categorie/sendhotel/'.$hotel) }}">
          <i class="fa fa-th"></i>
          <div>Administrar Categorias</div>
        </a>
      </div>
    </div>
  </div>
  <div class="col-12 text-center">
    <div class="title_list">
      <h1>Listado de Reservas @if(!empty($cancelada)) Canceladas @endif</h1>
    </div>
  </div>
  <div class="col-12">
    <div class="row content_center">
      <a href="{{ URL::to('/admin/booking/createwhithotel/'.$hotel) }}" class="btn btn-primary mr-2">Agregar Reserva</a>
      @if(!empty($cancelada))
      <a href="{{ URL::to('/admin/booking/sendhotel/'.$hotel) }}" class="btn btn-primary">Reservas Exitosas</a>
      @else
      <a href="{{ URL::to('/admin/booking/cancelledreservation/'.$hotel) }}" class="btn btn-primary">Reservas
        Canceladas</a>
      @endif
    </div>
    <table class="table table-hover">
      <thead>
        <tr>
          <th class="text-center">Estado</th>
          <th>Nombre</th>
          <th>Entrada</th>
          <th>Salida</th>
          @role('super-admin')
          <th class="col-xs-2 text-center">Acciones</th>
          @endrole
        </tr>
      </thead>
      <tbody>
        @foreach ($booking as $b)
        <tr>
          <td class="text-center">
            @if($b->state=='precheckin')
            <i class="fa fa-check-circle text-success" title="Aprobado Precheckin"></i>
            @elseif($b->state=='checkin')
            <i class="fa fa-check-circle text-success" title="Aprobado Checkin"></i>
            @elseif($b->state=='checkout')
            <i class="fa fa-times text-primary" title="Checkout"></i>
            @else
            <i class="fa fa-eye text-danger open-modal-reason" title="No Aprobado"
              data-id="<?php echo $b->reasontodelete; ?>"></i>
            @endif
          </td>
          <td>
            <?php 
                                    $n = explode('/', $b->name);
                                    echo $n[0];
                                ?>
          </td>
          <td>{{ $b->arrival_date }}</td>
          <td>{{ $b->departure_date }}</td>
          <?php
                                $occupiedroom = OccupiedRoom::select('name')->where('id_booking',$b->id)->get();
                            ?>
          <td class="text-center">
            <a class="btn_menu open-modal" id="btn_menu" href="#my_modal" data-toggle="modal"
              data-id="<div class='row'><div class='col-12'><div class='form-group'><b>Código Reserva: </b><span>{{ $b->reservation_code }}</span></div></div><div class='col-12'><div class='form-group'><b>Personas: </b><span>{{ $b->name }}</span></div></div><div class='col-12'><div class='form-group'><b>Correos: </b><span>{{ $b->email }}</span></div></div><div class='col-12'><div class='form-group'> <b>Celulares: </b><span>{{ $b->cellphone }}</span></div></div><div class='col-12'><div class='form-group'><b>Personas: </b><span>{{ $b->persons }}</span></div></div><div class='col-12'><div class='form-group'><b>Habitación: </b><span><?php foreach($occupiedroom as $or){ echo $or->name; } ?></span></div></div><div class='col-12'><div class='form-group'><b>Fecha Llegada: </b><span>{{ $b->arrival_date }}</span></div></div><div class='col-12'><div class='form-group'><b>Fecha Salida: </b><span>{{ $b->departure_date }}</span></div></div></div>">
              <i class="fa fa-user" title="Contacto"></i>
            </a>
            <a class="btn_menu" href="{{ url('/admin/booking/'.$b->id.'/edit') }}">
              <i class="fa fa-pencil" title="Editar"></i>
            </a>
            @if($b->state=='precheckin')
            <a class="btn_menu btn_delete open-modal-del" href="#" data-id="<?php echo $b->id; ?>">
              <i class="fa fa-trash" title="Eliminar"></i>
            </a>
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <input type="hidden" name="idselected" id="idselected">
  </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
  id="mi-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header content_message">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea eliminar la reserva?</b></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Info reason -->
<div class="modal fade" id="ModalReason" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Información Cancelación Reserva</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Info Contact -->
<div class="modal fade" id="ModalContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Información Contacto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-bodyc">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Reason -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
  id="reason-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel">Cual es el motivo para eliminar la reserva?</h5>
      </div>
      <div class="modal-body">
        <textarea name="reason" id="reason"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="modal-btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).on("click", ".open-modal", function () {
            var dataId = $(this).attr("data-id");
            $("#modal-bodyc").html(dataId);     

            $('#ModalContact').modal('show');
        });
        $(document).on("click", ".open-modal-del", function () {
            var dataId_ = $(this).attr("data-id");
            document.getElementById("idselected").value = dataId_; 
        });
        $(document).on("click", ".open-modal-reason", function () {
            var dataReason = $(this).attr("data-id");
            $("#modal-body").html(dataReason);     
            $('#ModalReason').modal('show');
        });
        var modalConfirm = function(callback){
          $(".btn_delete").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            $("#reason-modal").modal('show');
            //callback(true);
            $("#mi-modal").modal('hide');
          });

            $("#modal-btn-save").on("click", function(){
                var reasontodelete = $('#reason').val();
                saveReason(reasontodelete);
                callback(true);
                $("#reason-modal").modal('hide');
            });          
          
          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            $.ajax({
                url:"{{ route('deleteBooking') }}",
                method:"POST",
                data:{_token:_token,id:id},
                success:function(data){
                    location.reload();
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });
        function saveReason(reasontodelete){
            var _token = $('input[name="_token"]').val();
            var idb = $('#idselected').val();
            var rd = reasontodelete;
            $.ajax({
                url:"{{ route('saveReason') }}",
                method:"POST",
                data:{rd:rd,_token:_token,idb:idb},
                success:function(data){
                }
            });
        }
</script>
@stop