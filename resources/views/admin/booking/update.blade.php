@extends('layouts.platform')
@section('content')
    <div class="container_center">
        <div class="title_top">
            <a href="{{ URL::to('/admin/booking/sendhotel/'.$booking->id_hotel) }}">
                <i class="fa fa-sign-in"></i>
            </a>
            <h1 class="text-center">Editar Reserva</h1>
        </div>
        {!! Form::model($booking, [
            'method' => 'PATCH',
            'route' => ['booking.update', $booking->id],
            'enctype'=> 'multipart/from-data',
            'files'=> true,
            'name' => 'form_edit_booking'
        ]) !!}
                    
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información de la Reserva</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                    <input type="text" name="name" id="name" class="form-control input-lg" value="{{ $booking->name }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('identification', 'Identificación:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="identification" name="identification" value="{{ $booking->identification }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $booking->email }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('country', 'País:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="country" name="country" value="{{ $booking->country }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('city', 'Ciudad:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="city" name="city" value="{{ $booking->city }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('address', 'Dirección:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="address" name="address" value="{{ $booking->address }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('nationality', 'Nacionalidad:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="nationality" name="nationality" value="{{ $booking->nationality }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('gender', 'Género:', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="gender">
                                        <option value="ninguno" <?php if($booking->gender=='ninguno'){ echo "selected"; } ?>>-- Seleccione --</option>
                                        <option value="femenino" <?php if($booking->gender=='femenino'){ echo "selected"; } ?>>Femenino</option>
                                        <option value="masculino" <?php if($booking->gender=='masculino'){ echo "selected"; } ?>>Masculino</option>
                                        <option value="otros" <?php if($booking->gender=='otros'){ echo "selected"; } ?>>Otros</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('cellphone', 'Celular:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="cellphone" name="cellphone" value="{{ $booking->cellphone }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('birth', 'Fecha Cumpleaños:', ['class' => 'control-label']) !!}
                                    <input type="date" class="form-control" id="birth" name="birth" value="{{ $booking->birth }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('blood_type', 'Tipo de Sangre:', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="blood_type">
                                        <option value="ninguno" <?php if($booking->blood_type=='ninguno'){ echo "selected"; } ?>>-- Seleccione --</option>
                                        <option value="A -" <?php if($booking->blood_type=='A -'){ echo "selected"; } ?>>A -</option>
                                        <option value="A +" <?php if($booking->blood_type=='A +'){ echo "selected"; } ?>>A +</option>
                                        <option value="B -" <?php if($booking->blood_type=='B -'){ echo "selected"; } ?>>B -</option>
                                        <option value="B +" <?php if($booking->blood_type=='B +'){ echo "selected"; } ?>>B +</option>
                                        <option value="O -" <?php if($booking->blood_type=='O -'){ echo "selected"; } ?>>O -</option>
                                        <option value="O +" <?php if($booking->blood_type=='O +'){ echo "selected"; } ?>>O +</option>
                                        <option value="AB -" <?php if($booking->blood_type=='AB -'){ echo "selected"; } ?>>AB -</option>
                                        <option value="AB +" <?php if($booking->blood_type=='AB +'){ echo "selected"; } ?>>AB +</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('arrival_date', 'Fecha Llegada:', ['class' => 'control-label']) !!}
                                    <input id="datetimepicker_" class="form-control" id="arrival_date" name="arrival_date" value="{{ $booking->arrival_date }}" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('departure_date', 'Fecha Salida:', ['class' => 'control-label']) !!}
                                    <input id="datetimepicker" class="form-control" id="departure_date" name="departure_date" value="{{ $booking->departure_date }}" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('id_room_type', 'Habitación:', ['class' => 'control-label']) !!}
                                    <input type="text" name="idr" class="form-control" value="<?php foreach($occupiedroom as $or){ echo $or->name; } ?>">
                                    <select class="form-control booking_select" name="id_room_type" id="booking_select">
                                        <option value="0">-- Seleccione --</option>
                                        @foreach ($roomtype as $key => $value)
                                            <option value="{{ $value->id }}" <?php if($booking->id_room_type==$value->id){ echo "selected"; } ?>>{{ $value->type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('persons', 'Personas:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="persons" name="persons" value="{{ $booking->persons }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="price" name="price" value="{{ $booking->price }}">
                                </div>
                            </div>
                            <div class="col-12 p-0">
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('hotel_notes', 'Notas Hotel:', ['class' => 'control-label']) !!}
                                            <textarea class="form-control" id="hotel_notes" name="hotel_notes">{{ $booking->hotel_notes }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('guest_notes', 'Notas Huésped:', ['class' => 'control-label']) !!}
                                            <textarea class="form-control" id="guest_notes" name="guest_notes">{{ $booking->guest_notes }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center btn_save">
                <div class="justify-content-center">
                    <input type="hidden" name="id_hotel" value="{{ $booking->id_hotel }}">
                    {!! Form::submit('Editar Reserva', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection