<?php
    use App\Entities\Bookings\Booking;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
?>
@extends('layouts.platform')

@section('content')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"> </script>
    <?php $idh = Auth::user()->id_hotel; ?>
    <div class="row">
        <div class="col-12">
            <div class="text-center encabezados">
                <h3>Estadísticas</h3>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-12 center form_main">
                        <div class="row" style="margin-bottom:10px;">
                            <form autocomplete="off" style="width:100%;">
                                <div class="col-12 no_padding">
                                    <div class="row pt-4">
                                        <div class="col-4">
                                            <div class="row">
                                                <div class="col-12">
                                                    <span><b>Hotel</b></span>
                                                </div>
                                                <div class="col-12">
                                                    <select name="idhotel" id="idhotel" class="form-control">
                                                        @foreach($hotels as $h)
                                                            @if($idh!='0')
                                                                @if($idh==$h->id)
                                                                    <option value="{{ $h->id }}">{{ $h->name }}</option>
                                                                @endif
                                                            @else
                                                                <option value="{{ $h->id }}">{{ $h->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="row">
                                                <div class="col-12">
                                                    <span><b>Fecha inicial</b></span>
                                                </div>
                                                <div class="col-12">
                                                    <input type="date" name="fi" class="form-control" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="row">
                                                <div class="col-12">
                                                    <span><b>Fecha fin</b></span>
                                                </div>
                                                <div class="col-12">
                                                    <input type="date" name="ff" class="form-control" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                        <div class="row graficouno">
                            <div class="col-12 col-sm-6 columnados">
                                Reservas Exitosas
                            </div>
                            <div class="col-12 col-sm-6">
                                <?php 
                                    if($idh!=''){
                                        $output = ''; 
                                        $anioactual = date("Y");
                                        $mesactual = date("m");
                                        $consultaq = DB::select(DB::raw("SELECT price,created_at FROM bookings WHERE id_hotel='$idh' AND YEAR(created_at)='$anioactual' AND MONTH(created_at)='$mesactual'"));
                                        
                                    }else{
                                        $output = ''; 
                                        $anioactual = date("Y");
                                        $mesactual = date("m");
                                        $consultaq = DB::select(DB::raw("SELECT price,created_at FROM bookings WHERE YEAR(created_at)='$anioactual' AND MONTH(created_at)='$mesactual'"));
                                    }

                                    if($consultaq!=''){
                                        $output .= ' 
                                          <script>
                                            google.charts.load("current", {packages: ["corechart", "bar"]});
                                          google.charts.setOnLoadCallback(drawAxisTickColors);

                                          function drawAxisTickColors() {
                                                var data = google.visualization.arrayToDataTable([
                                                  ["Quincena 1", "Q.1"]';                      
                                        foreach($consultaq as $v){
                                            $fechahora = explode(' ', $v->created_at);
                                            $fecha = $fechahora[0];
                                            if($v->price!="")
                                                $output .= ',["'.$fecha.'", '.$v->price.']
                                                 ';
                                                
                                        }       
                                        $output .= ' ]);
                                                    var options = {
                                                      title: "Ingresos Reservas",
                                                      height: 400,
                                                      width: 400,
                                                      chartArea: {width: "40%"},
                                                      bar: {groupWidth: "80%"},
                                                      hAxis: {
                                                        title: "Total Ingresos Reservas",
                                                        minValue: 0,
                                                        textStyle: {
                                                          bold: true,
                                                          fontSize: 15,
                                                          color: "#4d4d4d"
                                                        },
                                                        titleTextStyle: {
                                                          bold: true,
                                                          fontSize: 15,
                                                          color: "#4d4d4d"
                                                        }
                                                      },
                                                      vAxis: {
                                                        title: "Fecha Reservas",
                                                        textStyle: {
                                                          fontSize: 12,
                                                          bold: true,
                                                          color: "#848484"
                                                        },
                                                        titleTextStyle: {
                                                          fontSize: 15,
                                                          bold: true,
                                                          color: "#848484"
                                                        }
                                                      }
                                                    };
                                                    
                                                    var chart = new google.visualization.BarChart(document.getElementById("chart_div"));
                                                    google.visualization.events.addListener(chart, "ready", function(){
                                                        var imgUri = chart.getImageURI();
                                                        /*document.getElementById("chartImg").src = imgUri;*/
                                                    });
                                                    chart.draw(data, options);
                                                  }
                                              </script>
                                        ';
                                    }else{
                                        echo "<br><br><center><h2>No hay Datos</h2></center>";
                                    }   
                                    echo $output;
                                ?>
                                <div id="chart_div"></div>
                                <!--<img id="chartImg" />-->
                            </div>
                        </div>
                    </div><!--form_main-->
                </div><!--row-->
            </div>
        </div>
    </div>
    <script>

        //$(document).ready(function(){
            /*
            $("#datepicker").datepicker({ format: "yyyy", viewMode: "years", minViewMode: "years" });
            $(".month").datepicker({ format: "mm", viewMode: "months", minViewMode: "months" });
            $('#idhotel').change(function(){
 
                document.getElementById('anioescogido').style.display = 'block';
                document.getElementById('datepicker').value = "";
                document.getElementById('month').value = "";
                var idhotel = $(this).val(); 
                var h = $("#idhotel option:selected").html();
                document.getElementById('n_modelo').innerHTML = nombrem;
                <?php //if($_SESSION["permisos"]=="admin"){ ?>
                    document.getElementById('exportar1').style.display = 'none';
                    document.getElementById('exportar2').style.display = 'block';
                    var s = document.getElementById('id_sede');
                    var id_sede = s.options[s.selectedIndex].value;
                    $.ajax({  
                        url:"estadistica_especifica.php",  
                        method:"POST",  
                        data:{nombre_modelo:nombre_modelo,id_sede:id_sede},  
                        success:function(data){  
                             $('#estadistica_especifica').html(data); 
                        }  
                    }); 
                <?php //}else{ ?>
                     $.ajax({  
                        url:"estadistica_especifica.php",  
                        method:"POST",  
                        data:{nombre_modelo:nombre_modelo},  
                        success:function(data){  
                             $('#estadistica_especifica').html(data); 
                        }  
                    });
                <?php //} ?>
            }); 
            $('#datepicker').change(function(){
                document.getElementById('mesescogido').style.display = 'block';
                $('#canva > canvas').remove();
            });
            $('#month').change(function(){
                document.getElementById('contenedor_porcentaje_quincena').style.border = 'solid 1px #919191';
                document.getElementById('cumplimiento1').style.display = 'block';
                document.getElementById('rojo').style.background = 'transparent';
                document.getElementById('rojo').style.width = '0'; 
                document.getElementById('naranja').style.background = 'transparent';
                document.getElementById('naranja').style.width = '0'; 
                document.getElementById('amarillo').style.background = 'transparent';
                document.getElementById('amarillo').style.width = '0'; 
                document.getElementById('verde').style.background = 'transparent';
                document.getElementById('verde').style.width = '0'; 

                document.getElementById('contenedor_porcentaje_quincena2').style.border = 'solid 1px #919191';
                document.getElementById('cumplimiento2').style.display = 'block';
                document.getElementById('rojo2').style.background = 'transparent';
                document.getElementById('rojo2').style.width = '0'; 
                document.getElementById('naranja2').style.background = 'transparent';
                document.getElementById('naranja2').style.width = '0'; 
                document.getElementById('amarillo2').style.background = 'transparent';
                document.getElementById('amarillo2').style.width = '0'; 
                document.getElementById('verde2').style.background = 'transparent';
                document.getElementById('verde2').style.width = '0'; 

                document.getElementById('chart_q1').innerHTML = "";
                document.getElementById('chart_q2').innerHTML = "";
                document.getElementById('columnchart_values').innerHTML = ""; 
                document.getElementById('columnchart2_values').innerHTML = "";  
                var nombre_modelo = document.getElementById('nombre_modelo').value;
                var anioe = document.getElementById('datepicker').value;
                var mese = $(this).val();
                <?php //if($_SESSION["permisos"]=="admin"){ ?>
                    var s = document.getElementById('id_sede');
                    var id_sede = s.options[s.selectedIndex].value;
                    $.ajax({  
                        url:"estadistica_especifica.php",  
                        method:"POST",  
                        data:{nombre_modelo:nombre_modelo, anioe:anioe, mese:mese,id_sede:id_sede},  
                        success:function(data){  
                             $('#estadistica_especifica').html(data); 
                        }  
                    });
                <?php //}else{ ?>
                    $.ajax({  
                        url:"estadistica_especifica.php",  
                        method:"POST",  
                        data:{nombre_modelo:nombre_modelo, anioe:anioe, mese:mese},  
                        success:function(data){  
                             $('#estadistica_especifica').html(data); 
                        }  
                    });
                <?php //} ?> 
            });
        });*/
    </script>
    <script type="text/javascript">
        $('.has-sub a').click(function(){
            $('.has-sub ul').hide();
            $(this).next().show();
        });
    </script>
@stop