@extends('layouts.platform')
@section('content')
<div class="container_center">
    <div class="title_top">
        <a href="{{ URL::to('/admin/booking/sendhotel/'.$h) }}">
            <i class="fa fa-sign-in"></i>
        </a>
        <h1 class="text-center">Nueva Reserva</h1>
    </div>
    {!! Form::open([
		'route' => 'booking.store',
        'method' => 'post',
        'enctype'=> 'multipart/form-data'
	]) !!}
    <div class="row">
        <div class="col-12">
            <div class="card content_form">
                <div class="card-header">
                    <h3>Información de la Reserva</h3>
                </div>
                <div class="card-body">
                    <h3>Persona Número 1</h3>
                    <div class="row duplicate">
                		<div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                <input type="text" name="name1" id="name1" class="form-control input-lg" required autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('identification', 'Identificación:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="identification1" name="identification1" onBlur="consultaDocumento();">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                                <input type="email1" class="form-control" id="email1" name="email1" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('country', 'País:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="country1" name="country1">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('city', 'Ciudad:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="city1" name="city1">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('address', 'Dirección:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="address1" name="address1" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('nationality', 'Nacionalidad:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="nationality1" name="nationality1">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('gender', 'Género:', ['class' => 'control-label']) !!}
                                <select class="form-control" name="gender1" id="gender1">
                                    <option value="ninguno">-- Seleccione --</option>
                                    <option value="femenino">Femenino</option>
                                    <option value="masculino">Masculino</option>
                                    <option value="otros">Otros</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('cellphone', 'Celular:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="cellphone1" name="cellphone1">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('birth', 'Fecha Cumpleaños:', ['class' => 'control-label']) !!}
                                <input type="date" class="form-control" id="birth1" name="birth1" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('blood_type', 'Tipo de Sangre:', ['class' => 'control-label']) !!}
                                <select class="form-control" name="blood_type1" id="blood_type1">
                                    <option value="ninguno">-- Seleccione --</option>
                                    <option value="A -">A -</option>
                                    <option value="A +">A +</option>
                                    <option value="B -">B -</option>
                                    <option value="B +">B +</option>
                                    <option value="O -">O -</option>
                                    <option value="O +">O +</option>
                                    <option value="AB -">AB -</option>
                                    <option value="AB +">AB +</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="show_item" id="dinamicos1">
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('persons', 'Personas:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="persons" name="persons" onBlur="cantidadPersonas();">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('arrival_date', 'Fecha Llegada:', ['class' => 'control-label']) !!}
                                <input id="datetimepicker_" class="form-control" id="arrival_date" name="arrival_date" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('departure_date', 'Fecha Salida:', ['class' => 'control-label']) !!}
                                <input id="datetimepicker" class="form-control" id="departure_date" name="departure_date" autocomplete="off" onBlur="habitacionEscogida();">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('id_room_type', 'Habitaciones Disponibles:', ['class' => 'control-label']) !!}
                                <select class="form-control" name="id_room" id="id_room" required onchange="habitacionEscogida()">
                                    <option value="0">-- Seleccione --</option>
                                </select>
                                <input type="hidden" name="quantity" class="quantity">
                                <input type="hidden" name="room_name" id="room_name">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="price" name="price" readonly>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-6 p-0">
                                    <div class="form-group">
                                        {!! Form::label('hotel_notes', 'Notas Hotel:', ['class' => 'control-label']) !!}
                                        <textarea class="form-control" id="hotel_notes" name="hotel_notes"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('guest_notes', 'Notas Huésped:', ['class' => 'control-label']) !!}
                                        <textarea class="form-control" id="guest_notes" name="guest_notes"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 text-center btn_save">
            <div class="justify-content-center">
                <input type="hidden" name="id_hotel" id="id_hotel" value="{{ $h }}">
                {!! Form::submit('Guardar Reserva', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- Modal Message -->
<div class="modal fade" id="ModalContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Información Personas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body">
        <p>Al ingresar la cantidad de personas se habilitan los campos para agregar la información personal de cada una de las personas que usarán la reserva.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<link href="{{ asset('css/jquery.datetimepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/jquery.datetimepicker.full.min.js') }}"></script>
<script>
    $('#datetimepicker_').datetimepicker({
      format:'Y-m-d H:i:s',
      lang:'es'
    });
    $('#datetimepicker').datetimepicker({
      format:'Y-m-d H:i:s',
      lang:'es'
    });
    function cantidadPersonas(){
        $('#ModalContact').modal('show');
        var personas = document.getElementById('persons').value;
        //document.getElementById('persons').readOnly = true;
        var i;
        for(i = 1; i < personas; i++){
            var cantidad = $('.duplicate').length + 1;
            var c = $('.duplicate').length;
            $('#dinamicos'+c).after(
                '<div id="dinamicos'+cantidad+'"><h3>Persona Número '+cantidad+'</h3><div class="row duplicate"><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label"> Nombre:</label><input type="text" name="name'+cantidad+'" id="name'+cantidad+'" class="form-control input-lg"/></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Identificación:</label><input type="text" class="form-control" id="identification'+cantidad+'" name="identification'+cantidad+'"></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Email:</label><input type="email" class="form-control" id="email'+cantidad+'" name="email'+cantidad+'"></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">País:</label><input type="text" class="form-control" id="country'+cantidad+'" name="country'+cantidad+'"></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Ciudad:</label><input type="text" class="form-control" id="city'+cantidad+'" name="city'+cantidad+'"></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Dirección:</label><input type="text" class="form-control" id="address'+cantidad+'" name="address'+cantidad+'"></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Nacionalidad:</label><input type="text" class="form-control" id="nationality'+cantidad+'" name="nationality'+cantidad+'"></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Género:</label><select class="form-control" name="gender'+cantidad+'"><option value="ninguno">-- Seleccione --</option><option value="femenino">Femenino</option><option value="masculino">Masculino</option><option value="otros">Otros</option></select></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Celular:</label><input type="text" class="form-control" id="cellphone'+cantidad+'" name="cellphone'+cantidad+'"></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Fecha Cumpleaños:</label><input type="date" class="form-control" id="birth'+cantidad+'" name="birth'+cantidad+'"></div></div><div class="col-12 col-sm-4"><div class="form-group"><label class="control-label">Tipo de Sangre:</label><select class="form-control" name="blood_type'+cantidad+'"><option value="ninguno">-- Seleccione --</option><option value="A -">A -</option><option value="A +">A +</option><option value="B -">B -</option><option value="B +">B +</option><option value="O -">O -</option><option value="O +">O +</option><option value="AB -">AB -</option><option value="AB +">AB +</option></select></div></div></div></div>');
        }
        var idh = document.getElementById('id_hotel').value;
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('buscarRooms') }}",
            method:"POST",
            data:{personas:personas,idh:idh,_token:_token},
            success:function(res){
                if(res == ''){
                    $("#id_room").empty();
                    $("#id_room").append('<option>No hay ninguna habitación con esta cantidad</option>');
                }else{
                    $("#id_room").empty();
                    $.each(res,function(key,value){
                        $("#id_room").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                    });
                    $("#id_room").prop('required',true);
                }
            }
        });
        
    }
    function habitacionEscogida(){
        var e = document.getElementById("id_room");
        var roomname = e.options[e.selectedIndex].text;
        document.getElementById("room_name").value = roomname;
        console.log(roomname);
        var query = $('#id_room').val();
        console.log(query);
        var llegada = $('#datetimepicker_').val();
        fechllegada = llegada.split(' ', 1);
        console.log(fechllegada);
        var salida = $('#datetimepicker').val();
        fechsalida = salida.split(' ', 1);
        console.log(fechsalida);
        if(llegada!='' && salida!=''){
            var date_1 = new Date(fechllegada);
            var date_2 = new Date(fechsalida);
            var day_as_milliseconds = 86400000;
            var diff_in_millisenconds = date_2 - date_1;
            var diff_in_days = diff_in_millisenconds / day_as_milliseconds;
            console.log(diff_in_days);
            if(query != ''){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('roomPrice') }}",
                    method:"POST",
                    data:{query:query,_token:_token},
                    success:function(price){
                        var p = diff_in_days * price;
                        document.getElementById("price").value = p;
                    }
                });
            }
        }
    }
    function consultaDocumento(){
        var d = $('#identification1').val();
        if(d != ''){
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:"{{ route('consultDocument') }}",
                method:"POST",
                data:{d:d,_token:_token},
                success:function(res){
                    if(res) {
                        $.each(res,function(key,value){
                            document.getElementById("name1").value = value['name'];
                            document.getElementById("email1").value = value['email'];
                            document.getElementById("country1").value = value['country'];
                            document.getElementById("city1").value = value['city'];
                            document.getElementById("address1").value = value['address'];
                            document.getElementById("nationality1").value = value['nationality'];
                            document.getElementById("gender1").value = value['gender'];
                            document.getElementById("cellphone1").value = value['cellphone'];
                            document.getElementById("birth1").value = value['birth'];
                            document.getElementById("blood_type1").value = value['blood_type'];
                        });
                    }
                }
            });
        }
    }
</script>
@stop
