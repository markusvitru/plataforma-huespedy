@extends('layouts.platform')
@section('content')
    <div class="container_center">
        <div class="title_top">
            <a href="{{ URL::to('/admin/guest/sendhotel/'.$guest->id_hotel) }}">
                <i class="fa fa-sign-in"></i>
            </a>
            <h1 class="text-center">Editar Huésped</h1>
        </div>
        {!! Form::model($guest, [
            'method' => 'PATCH',
            'route' => ['guest.update', $guest->id],
            'name' => 'form_edit_guest'
        ]) !!}
                    
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información del Huésped</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                    <input type="text" name="name" id="name" class="form-control input-lg" value="{{ $guest->name }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('identification', 'Identificación:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="identification" name="identification" value="{{ $guest->identification }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('type_identification', 'Tipo de Identificación:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="type_identification" name="type_identification" value="{{ $guest->type_identification }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $guest->email }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('country', 'País:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="country" name="country" value="{{ $guest->country }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('city', 'Ciudad:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="city" name="city" value="{{ $guest->city }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('address', 'Dirección:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="address" name="address" value="{{ $guest->address }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('gender', 'Nacionalidad:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="nationality" name="nationality" value="{{ $guest->nationality }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('cellphone', 'Celular:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="cellphone" name="cellphone" value="{{ $guest->cellphone }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('birth', 'Fecha Cumpleaños:', ['class' => 'control-label']) !!}
                                    <input type="date" class="form-control" id="birth" name="birth" value="{{ $guest->birth }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('gender', 'Género:', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="gender">
                                        <option value="ninguno" <?php if($guest->gender=='ninguno'){ echo "selected"; } ?>>-- Seleccione --</option>
                                        <option value="femenino" <?php if($guest->gender=='femenino'){ echo "selected"; } ?>>Femenino</option>
                                        <option value="masculino" <?php if($guest->gender=='masculino'){ echo "selected"; } ?>>Masculino</option>
                                        <option value="otros" <?php if($guest->gender=='otros'){ echo "selected"; } ?>>Otros</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('blood_type', 'Tipo de Sangre:', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="blood_type">
                                        <option value="ninguno" <?php if($guest->blood_type=='ninguno'){ echo "selected"; } ?>>-- Seleccione --</option>
                                        <option value="A -" <?php if($guest->blood_type=='A -'){ echo "selected"; } ?>>A -</option>
                                        <option value="A +" <?php if($guest->blood_type=='A +'){ echo "selected"; } ?>>A +</option>
                                        <option value="B -" <?php if($guest->blood_type=='B -'){ echo "selected"; } ?>>B -</option>
                                        <option value="B +" <?php if($guest->blood_type=='B +'){ echo "selected"; } ?>>B +</option>
                                        <option value="O -" <?php if($guest->blood_type=='O -'){ echo "selected"; } ?>>O -</option>
                                        <option value="O +" <?php if($guest->blood_type=='O +'){ echo "selected"; } ?>>O +</option>
                                        <option value="AB -" <?php if($guest->blood_type=='AB -'){ echo "selected"; } ?>>AB -</option>
                                        <option value="AB +" <?php if($guest->blood_type=='AB +'){ echo "selected"; } ?>>AB +</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center btn_save">
                <div class="justify-content-center">
                    <input type="hidden" name="id_hotel" value="{{ $guest->id_hotel }}">
                    {!! Form::submit('Editar Huésped', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection