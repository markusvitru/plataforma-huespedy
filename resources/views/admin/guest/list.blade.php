<?php
    use App\Entities\OccupiedRooms\OccupiedRoom;
    use Illuminate\Support\Facades\DB;
?>
@extends('layouts.platform')
@section('content')
<div class="row container_center">
    <div class="col-12 menu_hotel">
        <div class="row">
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/booking/sendhotel/'.$hotel) }}">
                    <i class="fa fa-calendar"></i>
                    <div>Administrar Reservas</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/roomtype/sendhotel/'.$hotel) }}">
                    <i class="fa fa-bed"></i>
                    <div>Administrar Habitaciones</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="#">
                    <i class="fa fa-users"></i>
                </a>
                <div>Administrar Huéspedes</div>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/consumeproduct/sendhotel/'.$hotel) }}">
                    <i class="fa fa-list-ol"></i>
                    <div>Consumos Huéspedes</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/product/sendhotel/'.$hotel) }}">
                    <i class="fa fa-cutlery"></i>
                    <div>Productos y Servicios</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/categorie/sendhotel/'.$hotel) }}">
                    <i class="fa fa-th"></i>
                    <div>Administrar Categorias</div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-12 text-center">
        <div class="title_list">
            <h1>Listado de Huéspedes</h1>
        </div>
    </div>
    <div class="col-12">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="text-center">Nombre</th>
                    <th>Habitación</th>
                    <th>Email</th>
                    <th>Nacionalidad</th>
                    <th>Celular</th>
                    @role('super-admin')
                    <th class="col-xs-2 text-center">Acciones</th>
                    @endrole
                </tr>
            </thead>
            <tbody>
                @foreach ($booking as $g)
                <tr>
                    <td>
                        <?php 
                                    $n = explode('/', $g->name);
                                    echo $n[0];
                                ?>
                    </td>
                    <td>
                        <?php 
                                    $occupiedroom = OccupiedRoom::select('*')->where('id_booking',$g->id)->get();
                                    foreach($occupiedroom as $value) {
                                        echo $value->name;
                                    }
                                ?>
                    </td>
                    <td>
                        <?php 
                                    $e = explode('/', $g->email);
                                    echo $e[0];
                                ?>
                    </td>
                    <td>
                        <?php 
                                    $nat = explode('/', $g->nationality);
                                    echo $nat[0];
                                ?>
                    </td>
                    <td>
                        <?php 
                                    $c = explode('/', $g->cellphone);
                                    echo $c[0];
                                ?>
                    </td>
                    <td class="text-center">
                        <a class="btn_menu" href="{{ url('/admin/booking/'.$g->id.'/edit') }}">
                            <i class="fa fa-pencil" title="Editar"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop