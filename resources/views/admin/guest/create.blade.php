<?php
	use App\User;
    use App\Guest;
?>
@extends('layouts.platform')


@section('content')
<div class="container_center">
    <div class="title_top">
        <a href="{{ URL::to('/admin/guest/sendhotel/'.$h) }}">
            <i class="fa fa-sign-in"></i>
        </a>
        <h1 class="text-center">Nuevo Huésped</h1>
    </div>
    {!! Form::open([
		'route' => 'guest.store',
        'method' => 'post',
        'enctype'=> 'multipart/form-data'
	]) !!}
    <div class="row">
        <div class="col-12">
            <div class="card content_form">
                <div class="card-header">
                    <h3>Información del Huésped</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                		<div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                <input type="text" name="name" id="name" class="form-control input-lg" required/>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('identification', 'Identificación:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="identification" name="identification">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('type_identification', 'Tipo de Identificación:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="type_identification" name="type_identification">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('country', 'País:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="country" name="country">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('city', 'Ciudad:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="city" name="city">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('address', 'Dirección:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="address" name="address">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('gender', 'Nacionalidad:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="nationality" name="nationality">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('cellphone', 'Celular:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="cellphone" name="cellphone">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('birth', 'Fecha Cumpleaños:', ['class' => 'control-label']) !!}
                                <input type="date" class="form-control" id="birth" name="birth">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('gender', 'Género:', ['class' => 'control-label']) !!}
                                <select class="form-control" name="gender">
                                    <option value="ninguno">-- Seleccione --</option>
                                    <option value="femenino">Femenino</option>
                                    <option value="masculino">Masculino</option>
                                    <option value="otros">Otros</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('blood_type', 'Tipo de Sangre:', ['class' => 'control-label']) !!}
                                <select class="form-control" name="blood_type">
                                    <option value="ninguno">-- Seleccione --</option>
                                    <option value="A -">A -</option>
                                    <option value="A +">A +</option>
                                    <option value="B -">B -</option>
                                    <option value="B +">B +</option>
                                    <option value="O -">O -</option>
                                    <option value="O +">O +</option>
                                    <option value="AB -">AB -</option>
                                    <option value="AB +">AB +</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 text-center btn_save">
            <div class="justify-content-center">
                <input type="hidden" name="id_hotel" value="{{ $h }}">
                {!! Form::submit('Guardar Huésped', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<link href="{{ asset('css/jquery.datetimepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/jquery.datetimepicker.full.min.js') }}"></script>
<script>
    //$('#datetimepicker').datetimepicker();
    $('#datetimepicker_').datetimepicker({
      format:'Y-m-d H:i:s',
      lang:'es'
    });
    $('#datetimepicker').datetimepicker({
      format:'Y-m-d H:i:s',
      lang:'es'
    });
</script>
@stop
