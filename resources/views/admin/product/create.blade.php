<?php
	use App\Entities\Categories\Category;
?>
@extends('layouts.platform')


@section('content')
<div class="container_center">
    <div class="title_top">
        <a href="{{ URL::to('/admin/product/sendhotel/'.$h) }}">
            <i class="fa fa-sign-in"></i>
        </a>
        <h1 class="text-center">Nueva Producto o Servicio</h1>
    </div>
    {!! Form::open([
		'route' => 'product.store',
        'method' => 'post',
        'enctype'=> 'multipart/form-data'
	]) !!}
    <div class="row">
        <div class="col-12">
            <div class="card content_form">
                <div class="card-header">
                    <h3>Información del Producto o Servicio</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                		<div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                <input type="text" name="name" id="name" class="form-control input-lg" required/>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('id_categoria', 'Categoría:', ['class' => 'control-label']) !!}
                                <?php
                                    $categorie = Category::get();
                                ?>
                                <select name="id_categoria" class="form-control">
                                    <option value="">-- Seleccione --</option> 
                                    <?php
                                        foreach( $categorie as $k => $objeto){ 
                                    ?>
                                            <option value="<?php echo $objeto['id']; ?>">
                                                <?php echo $objeto['name']; ?>
                                            </option> 
                                    <?php 
                                        } 
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="price" name="price">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                 {!! Form::label('available_before_ckeckin', 'Disponibilidad:', ['class' => 'control-label']) !!}
                                <div class="form-control sinborde">
                                    <input class="form-check-input" type="checkbox" value="si" id="available_before_ckeckin" name="available_before_ckeckin">
                                    <label class="form-check-label" for="available_before_ckeckin">
                                        Disponible antes del checkin
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('schedule', 'Horarios:', ['class' => 'control-label']) !!}
                                <textarea class="form-control" name="schedule"></textarea>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('details', 'Detalles:', ['class' => 'control-label']) !!}
                                <textarea class="form-control" name="details"></textarea>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('url_imgs', 'Adjuntar Imagen:', ['class' => 'control-label']) !!}
                                <input id="url_imgs" class="form-control" name="url_imgs" type="file">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 text-center btn_save">
            <div class="justify-content-center">
                <input type="hidden" name="id_hotel" value="{{ $h }}">
                {!! Form::submit('Guardar Producto', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop
