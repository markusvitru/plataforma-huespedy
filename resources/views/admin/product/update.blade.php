<?php
    use App\Entities\Categories\Category;
?>
@extends('layouts.platform')
@section('content')
    <div class="container_center">
        <div class="title_top">
            <a href="{{ URL::to('/admin/product/sendhotel/'.$product->id_hotel) }}">
                <i class="fa fa-sign-in"></i>
            </a>
            <h1 class="text-center">Editar Producto</h1>
        </div>
        {!! Form::model($product, [
            'method' => 'PATCH',
            'route' => ['product.update', $product->id],
            'enctype'=> 'multipart/from-data',
            'files'=> true,
            'name' => 'form_edit_product'
        ]) !!}
                    
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información del Producto</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                    		<div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                    <input type="text" name="name" id="name" class="form-control" value="{{ $product->name }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('id_categoria', 'Categoría:', ['class' => 'control-label']) !!}
                                    <?php
                                        $categorie = Category::get();
                                    ?>
                                    <select name="id_categoria" class="form-control">
                                        <option value="">-- Seleccione --</option> 
                                        <?php
                                            foreach( $categorie as $k => $objeto){ 
                                        ?>
                                                <option value="<?php echo $objeto['id']; ?>" <?php if( $product->id_categoria==$objeto['id']){ echo 'selected';} ?>>
                                                    <?php echo $objeto['name']; ?>
                                                </option> 
                                        <?php 
                                            } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="price" name="price" value="{{ $product->price }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                     {!! Form::label('available_before_ckeckin', 'Disponibilidad:', ['class' => 'control-label']) !!}
                                    <div class="form-control sinborde">
                                        <input class="form-check-input" type="checkbox" value="si" id="available_before_ckeckin" name="available_before_ckeckin" value="{{ $product->available_before_ckecki }}">
                                        <label class="form-check-label" for="available_before_ckeckin">
                                            Disponible antes del checkin
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('schedule', 'Horarios:', ['class' => 'control-label']) !!}
                                    <textarea class="form-control" name="schedule">{{ $product->schedule }}</textarea>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('details', 'Detalles:', ['class' => 'control-label']) !!}
                                    <textarea class="form-control" name="details">{{ $product->details }}</textarea>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                {!! Form::label('url_imgs', 'Imagen Producto:', ['class' => 'control-label']) !!}
                                <div class="form-group">
                                    <?php 
                                        if($product->url_imgs!=''){
                                    ?>
                                        <div class="img-wrap">
                                            <span class="close">&times;</span>
                                            <img src="{{asset('images/products/'.$product->url_imgs)}}" class="img-fluid">
                                            <input type="hidden" name="url_imgs" class="url_imgs" value="{{ $product->url_imgs }}">
                                            <input type="hidden" name="id_product" class="id_product" value="{{ $product->id }}">
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group">
                                                {!! Form::label('url_imgs', 'Adjuntar Imagen:', ['class' => 'control-label']) !!}
                                                <input id="url_imgs" class="form-control" name="url_imgs" type="file">
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-12 text-center btn_save">
                                <div class="justify-content-center">
                                    <input type="hidden" name="id_hotel" value="{{ $product->id_hotel }}">
                                    {!! Form::submit('Editar Producto', ['class' => 'btn btn-primary']) !!}

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Modal eliminar logo -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Realmente desea eliminar la imagen?</h4>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
          </div>
        </div>
      </div>
    </div>
    <script>
        $(document).ready(function(){
            var modalConfirm = function(callback){
              $(".img-wrap .close").on("click", function(){
                $("#mi-modal").modal('show');
              });
              $("#modal-btn-si").on("click", function(){
                callback(true);
                $("#mi-modal").modal('hide');
              });
              $("#modal-btn-no").on("click", function(){
                callback(false);
                $("#mi-modal").modal('hide');
              });
            };
            modalConfirm(function(confirm){
              if(confirm){
                //Acciones si el usuario confirma
                var query = $('.url_imgs').val();
                if(query != ''){
                    var _token = $('input[name="_token"]').val();
                    var idproduct = $('.id_product').val();
                    $.ajax({
                        url:"{{ route('deleteimg') }}",
                        method:"POST",
                        data:{query:query,idproduct:idproduct,_token:_token},
                        success:function(data){
                            location.reload();
                        }
                    });
                }
              }else{
                //Acciones si el usuario no confirma
                $("#result").html("NO CONFIRMADO");
              }
            });
            
        });
    </script>
@endsection