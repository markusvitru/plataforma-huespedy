<?php
    use App\Entities\OccupiedRooms\OccupiedRoom;
    use Illuminate\Support\Facades\DB;
?>
@extends('layouts.platform')
@section('content')
<div class="container_center">
    <div class="title_top">
        <a href="{{ URL::to('/admin/consumeproduct/sendhotel/'.$h) }}">
            <i class="fa fa-sign-in"></i>
        </a>
        <h1 class="text-center">Editar Consumo de Productos</h1>
    </div>
    {!! Form::model($consumeproduct, [
    'method' => 'PATCH',
    'route' => ['consumeproduct.update', $consumeproduct->id],
    'name' => 'form_edit_consumeproduct'
    ]) !!}

    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card content_form">
                <div class="card-header">
                    <h3>Información del Consumo de Productos</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                <input type="text" name="name" id="name" class="form-control input-lg"
                                    value="{{ $consumeproduct->name }}" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('id_booking', 'Huésped:', ['class' => 'control-label']) !!}
                                <select name="id_booking" id="id_booking" class="form-control"
                                    onBlur="recogerNombre();">
                                    <option value="">-- Seleccione --</option>
                                    @foreach($booking as $b)
                                    <?php
                                                $v = explode(' / ', $b->name);
                                                $cant = count($v);
                                                if($cant=='1'){
                                            ?>
                                    <option value="{{ $b->id }}"
                                        <?php if($consumeproduct->id_booking==$b->id){ echo 'selected'; } ?>>
                                        {{ $b->name }}
                                    </option>
                                    <?php }else{ 
                                                    for($i=0;$i<$cant;$i++){

                                            ?>
                                    <option value="{{ $b->id }}"
                                        <?php if($consumeproduct->id_booking==$b->id && $consumeproduct->name_booking==$v[$i]){ echo 'selected'; } ?>>
                                        {{ $v[$i] }}
                                    </option>
                                    <?php }} ?>
                                    @endforeach
                                </select>
                                <input type="hidden" name="name_booking" id="name_booking">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {!! Form::label('room', 'Habitación:', ['class' => 'control-label']) !!}
                                <input type="text" name="room" id="room" class="form-control input-lg" value="<?php 
                                    $occupiedroom = OccupiedRoom::select('*')->where('id_booking',$consumeproduct->id_booking)->get();
                                    foreach($occupiedroom as $value) {
                                        echo $value->name;
                                    }
                                ?>" readonly />
                            </div>
                        </div>
                        <div class="col-12">
                            <?php 
                                    $v = explode('-', $consumeproduct->id_products); 
                                    for($i=0;$i<$cant;$i++){
                                ?>
                            <h3>Producto Número {{ $i+1 }}</h3>
                            <div class="row duplicate">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('id_products', 'Producto:', ['class' => 'control-label']) !!}
                                        <select name="id_product<?php echo $i+1; ?>" id="id_product<?php echo $i+1; ?>"
                                            class="form-control" onBlur="calcularPrecio()">
                                            <option value="">-- Seleccione --</option>
                                            @foreach($product as $k => $p)
                                            <option value="{{ $p->id }}"
                                                <?php if($p->id==$v[$i]){ echo 'selected'; } ?>>
                                                {{ $p->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                        <input type="text" class="form-control" id="price<?php echo $i+1; ?>"
                                            name="price<?php echo $i+1; ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="show_item" id="dinamicos1">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('total', 'Total:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="total" name="total" readonly
                                    value="{{ $consumeproduct->total }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('state', 'Estado:', ['class' => 'control-label']) !!}
                                <select name="state" class="form-control">
                                    <option value="">-- Seleccione --</option>
                                    <option value="pendientes"
                                        <?php if($consumeproduct->state=='pendientes'){ echo 'selected'; } ?>>Pendientes
                                    </option>
                                    <option value="aprobados"
                                        <?php if($consumeproduct->state=='aprobados'){ echo 'selected'; } ?>>Aprobados
                                    </option>
                                    <option value="anulados"
                                        <?php if($consumeproduct->state=='anulados'){ echo 'selected'; } ?>>Anulados
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                {!! Form::label('notes', 'Notas:', ['class' => 'control-label']) !!}
                                <textarea class="form-control" name="notes">{{ $consumeproduct->notes }}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 text-center btn_save">
            <div class="justify-content-center">
                <input type="hidden" name="id_hotel" value="">
                {!! Form::submit('Editar Consumo de Productos', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
            var c = $('.duplicate').length;
            var entra = 0;
            for(var i=1;i<=c;i++){
                var ip = "id_product"+i;
                var e = document.getElementById(ip);
                var idproduct = e.options[e.selectedIndex].value;
                if(idproduct != ''){
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url:"{{ route('productPrice') }}",
                        method:"POST",
                        data:{query:idproduct,_token:_token},
                        success:function(price){
                            for(var j=1;j<=c;j++){
                                var p = price;
                                var ent = entra+1; 
                                console.log("price"+j);
                                document.getElementById("price"+j).value = p;
                                /*var pr = parseInt(p);
                                total = pr+total;
                                document.getElementById("total").value = total;*/
                            }
                        }
                    });
                }
            } 
        });
</script>
@endsection