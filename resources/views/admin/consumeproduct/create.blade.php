<?php
	use App\Entities\ConsumeProducts\ConsumeProduct;
    use App\Entities\Products\Product;
?>
@extends('layouts.platform')
@section('content')
<div class="container_center">
    <div class="title_top">
        <a href="{{ URL::to('/admin/consumeproduct/sendhotel/'.$h) }}">
            <i class="fa fa-sign-in"></i>
        </a>
        <h1 class="text-center">Nuevo Consumo de Productos</h1>
    </div>
    {!! Form::open([
    'route' => 'consumeproduct.store',
    'method' => 'post',
    'enctype'=> 'multipart/form-data'
    ]) !!}
    <div class="row">
        <div class="col-12">
            <div class="card content_form">
                <div class="card-header">
                    <h3>Información Consumo de Productos</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                <input type="text" name="name" id="name" class="form-control input-lg" required />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('id_booking', 'Huésped:', ['class' => 'control-label']) !!}
                                @if(!empty($booking))
                                <select name="id_booking" id="id_booking" class="form-control"
                                    onBlur="recogerNombre();">
                                    <option value="">-- Seleccione --</option>
                                    @foreach($booking as $b)
                                    <?php
                                                $v = explode('/', $b->name);
                                                $cant = count($v);
                                                if($cant=='1'){
                                            ?>
                                    <option value="{{ $b->id }}">
                                        {{ $b->name }}
                                    </option>
                                    <?php }else{ 
                                                    for($i=0;$i<$cant;$i++){

                                            ?>
                                    <option value="{{ $b->id }}">
                                        {{ $v[$i]}}
                                    </option>
                                    <?php }} ?>
                                    @endforeach
                                </select>
                                @else
                                <div class="form-control">
                                    No hay huéspedes registrados
                                </div>
                                @endif
                                <input type="hidden" name="name_booking" id="name_booking">
                            </div>
                        </div>
                        <div class="col-12">
                            <h3>Producto Número 1</h3>
                            <div class="row duplicate">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('id_products', 'Producto:', ['class' => 'control-label']) !!}
                                        @if(!empty($product))
                                        <select name="id_product1" id="id_product1" class="form-control"
                                            onBlur="calcularPrecio()">
                                            <option value="">-- Seleccione --</option>
                                            @foreach($product as $p)
                                            <option value="{{ $p->id }}">
                                                {{ $p->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @else
                                        <div class="form-control">
                                            No hay productos registrados
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                        <input type="text" class="form-control" id="price1" name="price1" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="show_item" id="dinamicos1">
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <div class="btn btn-primary" id="room_type" onclick="detectarEstado();">Agregar Otros
                                Productos
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('total', 'Total:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="total" name="total" readonly>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('state', 'Estado:', ['class' => 'control-label']) !!}
                                <select name="state" class="form-control">
                                    <option value="">-- Seleccione --</option>
                                    <option value="pendientes">Pendientes</option>
                                    <option value="aprobados">Aprobados</option>
                                    <option value="anulados">Anulados</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                {!! Form::label('notes', 'Notas:', ['class' => 'control-label']) !!}
                                <textarea class="form-control" name="notes"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 text-center btn_save">
            <div class="justify-content-center">
                <input type="hidden" name="id_hotel" value="{{ $h }}">
                {!! Form::submit('Guardar Consumo de Productos', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    function detectarEstado(){
        var cantidad = $('.duplicate').length + 1;
        var c = $('.duplicate').length;
        $('#dinamicos'+c).after('<div id="dinamicos'+cantidad+'"><h3>Producto Número '+cantidad+'</h3><div class="row duplicate"><div class="col-12 col-sm-6"><div class="form-group"><label class="control-label">Producto:</label>@if(!empty($product))<select name="id_product'+cantidad+'" id="id_product'+cantidad+'" class="form-control" onBlur="calcularPrecio()"><option value="">-- Seleccione --</option>@foreach($product as $p)<option value="{{ $p->id }}">{{ $p->name }}</option>@endforeach</select>@else<div class="form-control">No hay productos registrados</div>@endif</div></div><div class="col-12 col-sm-6"><div class="form-group"><label class="control-label">Precio:</label><input type="text" class="form-control" id="price'+cantidad+'" name="price'+cantidad+'" readonly></div></div></div></div>');
    }
    function recogerNombre(){
        var e = document.getElementById("id_booking");
        var roomname = e.options[e.selectedIndex].text;
        document.getElementById("name_booking").value = roomname;
    }
    var total=0;
    function calcularPrecio(){
        var c = $('.duplicate').length;
        var ip = "id_product"+c;
        var e = document.getElementById(ip);
        var idproduct = e.options[e.selectedIndex].value;
        if(idproduct != ''){
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:"{{ route('productPrice') }}",
                method:"POST",
                data:{query:idproduct,_token:_token},
                success:function(price){
                    var p = price;
                    document.getElementById("price"+c).value = p;
                    var pr = parseInt(p);
                    total = pr+total;
                    document.getElementById("total").value = total;
                }
            });
        }
    }
</script>
@stop