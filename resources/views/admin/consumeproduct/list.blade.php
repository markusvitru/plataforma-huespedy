<?php
    use App\Entities\OccupiedRooms\OccupiedRoom;
    use Illuminate\Support\Facades\DB;
?>
@extends('layouts.platform')

@section('content')
<div class="row container_center">
    <div class="col-12 menu_hotel">
        <div class="row">
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/booking/sendhotel/'.$hotel) }}">
                    <i class="fa fa-calendar"></i>
                    <div>Administrar Reservas</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/roomtype/sendhotel/'.$hotel) }}">
                    <i class="fa fa-bed"></i>
                    <div>Administrar Habitaciones</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/guest/sendhotel/'.$hotel) }}">
                    <i class="fa fa-users"></i>
                    <div>Administrar Huéspedes</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="#">
                    <i class="fa fa-list-ol"></i>
                </a>
                <div>Consumos Huéspedes</div>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/product/sendhotel/'.$hotel) }}">
                    <i class="fa fa-cutlery"></i>
                    <div>Productos y Servicios</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/categorie/sendhotel/'.$hotel) }}">
                    <i class="fa fa-th"></i>
                    <div>Administrar Categorias</div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-12 text-center">
        <div class="title_list">
            <h1>Listado de Consumo de Productos</h1>
        </div>
    </div>
    <div class="col-12">
        <div class="row content_center">
            <a href="{{ URL::to('/admin/consumeproduct/createwhithotel/'.$hotel) }}" class="btn btn-primary">Agregar
                Consumo de Productos</a>
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Estado</th>
                    <th>Nombre</th>
                    <th>Huésped</th>
                    <th>Habitación</th>
                    <th>Fecha</th>
                    <th>Valor</th>
                    @role('super-admin')
                    <th class="col-xs-2 text-right">Acciones</th>
                    @endrole
                </tr>
            </thead>
            <tbody>
                @foreach ($consumeproduct as $cp)
                <tr>
                    <td>{{ $cp->state }}</td>
                    <td>{{ $cp->name }}</td>
                    <td>{{ $cp->name_booking }}</td>
                    <td>
                        <?php 
                                    $occupiedroom = OccupiedRoom::select('*')->where('id_booking',$cp->id_booking)->get();
                                    foreach($occupiedroom as $value) {
                                        echo $value->name;
                                    }
                                ?>
                    </td>
                    <td>{{ $cp->created_at }}</td>
                    <td>{{ $cp->total }}</td>
                    <td class="text-right">
                        <a class="btn_menu" href="{{ url('/admin/consumeproduct/'.$cp->id.'/edit') }}">
                            <i class="fa fa-pencil" title="Editar"></i>
                        </a>
                        <a class="btn_menu btn_delete" href="#">
                            <i class="fa fa-trash" title="Eliminar"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
    id="mi-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header content_message">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea eliminar el consumo de
                        productos?</b></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
                <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
            </div>
        </div>
    </div>
</div>
<script>
    var modalConfirm = function(callback){
          $(".btn_delete").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });
          
          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var id = $('.id_h').val();
            $.ajax({
                success:function(data){
                    location.reload();
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });
</script>
@stop