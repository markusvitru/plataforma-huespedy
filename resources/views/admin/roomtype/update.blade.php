@extends('layouts.platform')
@section('content')
    <div class="container_center">
        <div class="title_top">
            <a href="{{ URL::to('/admin/roomtype/sendhotel/'.$roomtype->id_hotel) }}">
                <i class="fa fa-sign-in"></i>
            </a>
            <h1 class="text-center">Editar Habitación</h1>
        </div>
        {!! Form::model($roomtype, [
            'method' => 'PATCH',
            'route' => ['roomtype.update', $roomtype->id],
            'name' => 'form_edit_roomtype'
        ]) !!}
                    
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información de la Habitación</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('capacity', 'Capacidad:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="capacity" name="capacity" value="{{ $roomtype->capacity }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="price" name="price" value="{{ $roomtype->price }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('quantity', 'Cantidad de este Tipo:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="quantity" name="quantity" value="{{ $roomtype->quantity }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('availability', 'Cantidad Disponibles:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="availability" name="availability" value="{{ $roomtype->availability }}">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    {!! Form::label('type', 'Número o Nombre:', ['class' => 'control-label']) !!}
                                    <textarea name="type" id="type" class="form-control"/>{{ $roomtype->type }}</textarea>
                                    <small class="text-danger">Por favor agregue cada uno de los nombres o números de las habitaciones de este tipo separados por un guion medio(-). Recuerde que dependiendo de la CANTIDAD DE ESTE TIPO ingresada es la cantidad de nombres que debe agregar, uno por cada habitación de este tipo.</small>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    {!! Form::label('observation', 'Observaciones:', ['class' => 'control-label']) !!}
                                    <textarea class="form-control" name="observation">{{ $roomtype->observation }}</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center btn_save">
                <div class="justify-content-center">
                    <input type="hidden" name="id_hotel" value="{{ $roomtype->id_hotel }}">
                    {!! Form::submit('Editar Habitación', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection