<?php
    use App\Entities\OccupiedRooms\OccupiedRoom;
    use App\Entities\Rooms\Room;
?>
@extends('layouts.platform')

@section('content')
<div class="row container_center">
    <div class="col-12 menu_hotel">
        <div class="row">
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/booking/sendhotel/'.$hotel) }}">
                    <i class="fa fa-calendar"></i>
                    <div>Administrar Reservas</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="#">
                    <i class="fa fa-bed"></i>
                </a>
                <div>Administrar Habitaciones</div>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/guest/sendhotel/'.$hotel) }}">
                    <i class="fa fa-users"></i>
                    <div>Administrar Huéspedes</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/consumeproduct/sendhotel/'.$hotel) }}">
                    <i class="fa fa-list-ol"></i>
                    <div>Consumos Huéspedes</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/product/sendhotel/'.$hotel) }}">
                    <i class="fa fa-cutlery"></i>
                    <div>Productos y Servicios</div>
                </a>
            </div>
            <div class="col-2 text-center">
                <a class="btn_menu" href="{{ URL::to('/admin/categorie/sendhotel/'.$hotel) }}">
                    <i class="fa fa-th"></i>
                    <div>Administrar Categorias</div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-12 text-center">
        <div class="title_list">
            <h1>Listado de Habitaciones</h1>
        </div>
    </div>
    <div class="col-12">
        <div class="row content_center">
            <a href="{{ URL::to('/admin/roomtype/createwhithotel/'.$hotel) }}" class="btn btn-primary">Agregar
                Habitación</a>
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="text-center">Estado</th>
                    <th>Nombre o Número</th>
                    <th class="text-center">Capacidad</th>
                    <th class="text-center">Disponibilidad</th>
                    <th class="text-center">Valor</th>
                    @role('super-admin')
                    <th class="col-xs-2 text-center">Acciones</th>
                    @endrole
                </tr>
            </thead>
            <tbody>
                @foreach ($roomtype as $rt)
                <?php 
                            $idrt = $rt->id;
                            $room = Room::select('*')->where('id_room_type',$idrt)->get();
                        ?>
                @foreach ($room as $r)
                <?php
                                $occupiedroom = OccupiedRoom::select('id')->where('id_room',$r->id)->get();
                            ?>
                <tr>
                    <td class="text-center">
                        @if($occupiedroom->isEmpty())
                        <i class="fa fa-check-circle text-success" title="Habitación Disponible"></i>
                        @else
                        <i class="fa fa-eye text-danger" title="Habitación No Disponible"></i>
                        @endif
                    </td>
                    <td>{{ $r->name }}</td>
                    <td class="text-center">{{ $rt->capacity }}</td>
                    <td class="text-center">{{ $rt->availability }}</td>
                    <td class="text-center">{{ $rt->price }}</td>
                    <td class="text-center">
                        <a class="btn_menu" href="{{ url('/admin/roomtype/'.$rt->id.'/edit') }}">
                            <i class="fa fa-pencil" title="Editar"></i>
                        </a>
                        <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $rt->id }}">
                            <i class="fa fa-trash" title="Eliminar"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @endforeach
            </tbody>
        </table>
        <input type="hidden" name="idselected" id="idselected">
    </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
    id="mi-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header content_message">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea eliminar la
                        habitación?</b></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
                <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click", ".open-modal-del", function () {
            var dataId_ = $(this).attr("data-id");
            document.getElementById("idselected").value = dataId_; 
        });
        var modalConfirm = function(callback){
          $(".btn_delete").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });
          
          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            $.ajax({
                url:"{{ route('deleteRoomType') }}",
                method:"POST",
                data:{_token:_token,id:id},
                success:function(data){
                    location.reload();
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });
</script>
@stop