<?php
    use App\User;
    use App\Entities\RoomTypes\RoomType;
?>
@extends('layouts.platform')

@section('content')
<div class="container_center">
    <div class="title_top">
        <a href="{{ URL::to('/admin/roomtype/sendhotel/'.$h) }}">
            <i class="fa fa-sign-in"></i>
        </a>
        <h1 class="text-center">Nueva Habitación</h1>
    </div>
    {!! Form::open([
    'route' => 'roomtype.store',
    'method' => 'post',
    'enctype'=> 'multipart/form-data'
    ]) !!}
    <div class="row">
        <div class="col-12">
            <div class="card content_form">
                <div class="card-header">
                    <h3>Información Tipos de Habitación</h3>
                </div>
                <div class="card-body">
                    <h3>Habitación Número 1</h3>
                    <div class="row duplicate">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('capacity', 'Capacidad:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="capacity1" name="capacity1">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="price1" name="price1">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('quantity', 'Cantidad de este Tipo:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="quantity1" name="quantity1">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('availability', 'Cantidad Disponibles:', ['class' => 'control-label'])
                                !!}
                                <input type="text" class="form-control" id="availability1" name="availability1">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                {!! Form::label('type', 'Número o Nombre:', ['class' => 'control-label']) !!}
                                <textarea name="type1" id="type1" class="form-control"
                                    placeholder="Ejemplo: Habitación 1 - Habitación 2 - Habitación 3 ......"
                                    required /></textarea>
                                <small class="text-danger">Por favor agregue cada uno de los nombres o números de las
                                    habitaciones de este tipo separados por un guion medio(-). Recuerde que dependiendo
                                    de la CANTIDAD DE ESTE TIPO ingresada es la cantidad de nombres que debe agregar,
                                    uno por cada habitación de este tipo.</small>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                {!! Form::label('observation', 'Observaciones:', ['class' => 'control-label']) !!}
                                <textarea class="form-control" name="observation1"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="show_item" id="dinamicos1">
                    </div>
                    <div class="col-12 text-center">
                        <div class="btn btn-primary" id="room_type" onclick="detectarEstado();">Agregar Otros Tipos de
                            Habitación
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 text-center btn_save">
            <div class="justify-content-center">
                <input type="hidden" name="id_hotel" value="{{ $h }}">
                {!! Form::submit('Guardar Habitación', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    function detectarEstado(){
        var cantidad = $('.duplicate').length + 1;
        var c = $('.duplicate').length;
        $('#dinamicos'+c).after(
            '<div id="dinamicos'+cantidad+'"><h3>Habitación Número '+cantidad+'</h3><div class="row duplicate"><div class="col-12 col-sm-6"><div class="form-group"><label class="control-label">Capacidad:</label><input type="text" class="form-control" id="capacity'+cantidad+'" name="capacity'+cantidad+'"></div></div><div class="col-12 col-sm-6"><div class="form-group"><label class="control-label">Precio:</label><input type="text" class="form-control" id="price'+cantidad+'" name="price'+cantidad+'"></div></div><div class="col-12 col-sm-6"><div class="form-group"><label class="control-label">Cantidad de este Tipo:</label><input type="text" class="form-control" id="quantity'+cantidad+'" name="quantity'+cantidad+'"></div></div><div class="col-12 col-sm-6"><div class="form-group"><label class="control-label">Cantidad Disponibles:</label><input type="text" class="form-control" id="availability'+cantidad+'" name="availability'+cantidad+'"></div></div><div class="col-12"><div class="form-group"><label class="control-label">Número o Nombre:</label><textarea name="type'+cantidad+'" id="type'+cantidad+'" class="form-control" placeholder="Ejemplo: Habitación 1 - Habitación 2 - Habitación 3 ......" required/></textarea> <small class="text-danger">Por favor agregue cada uno de los nombres o números de las habitaciones de este tipo separados por un guion medio(-). Recuerde que dependiendo de la CANTIDAD DE ESTE TIPO ingresada es la cantidad de nombres que debe agregar, uno por cada habitación de este tipo.</small></div></div><div class="col-12"><div class="form-group"><label class="control-label">Observaciones:</label><textarea class="form-control" name="observation'+cantidad+'"></textarea></div></div></div>'
        );
    }
</script>
@stop