<?php
    use App\Entities\Categories\Category;
    use App\User;
?>
@extends('layouts.platform')

@section('content')
    <div class="row container_center">
        <div class="col-12 menu_hotel">
            <div class="row">
                <div class="col-2 text-center">
                    <a class="btn_menu" href="{{ URL::to('/admin/booking/sendhotel/'.$hotel) }}">
                        <i class="fa fa-calendar"></i>
                        <div>Administrar Reservas</div>
                    </a>
                </div>
                <div class="col-2 text-center">
                    <a class="btn_menu" href="{{ URL::to('/admin/roomtype/sendhotel/'.$hotel) }}">
                        <i class="fa fa-bed"></i>
                        <div>Administrar Habitaciones</div>
                    </a>
                </div>
                <div class="col-2 text-center">
                    <a class="btn_menu" href="{{ URL::to('/admin/guest/sendhotel/'.$hotel) }}">
                        <i class="fa fa-users"></i>
                        <div>Administrar Huéspedes</div>
                    </a>
                </div>
                <div class="col-2 text-center">
                    <a class="btn_menu" href="{{ URL::to('/admin/consumeproduct/sendhotel/'.$hotel) }}">
                        <i class="fa fa-list-ol"></i>
                        <div>Consumos Huéspedes</div>
                    </a>
                </div>
                <div class="col-2 text-center">
                    <a class="btn_menu" href="{{ URL::to('/admin/product/sendhotel/'.$hotel) }}">
                        <i class="fa fa-cutlery"></i>
                        <div>Productos y Servicios</div>
                    </a>
                </div>
                <div class="col-2 text-center">
                    <a class="btn_menu" href="#">
                        <i class="fa fa-th"></i>
                    </a>
                    <div>Administrar Categorias</div>
                </div>
            </div>
        </div>
        <div class="col-12 text-center">
            <div class="title_list">
                <h1>Listado de Categorias</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="row content_center">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalForm">Agregar Categoria</button>
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        @role('super-admin')
                            <th class="col-xs-2 text-right">Acciones</th>
                        @endrole
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categorie as $c)
                        <tr>
                            <td>{{ $c->name }}</td>
                            <td class="text-right">
                                <a class="btn_menu" href="{{ url('/admin/categorie/'.$c->id.'/edit') }}">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                </a>
                                <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $c->id }}">
                                    <i class="fa fa-trash" title="Eliminar"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <input type="hidden" name="idselected" id="idselected">
        </div>
    </div>
    <!-- Modal Save Categoria -->
    <div class="modal fade" id="modalForm" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
             <!-- Encabezado modal --> 
                <div class="modal-header">
                    <button type="button" class="close m-0" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Agregar Categoria </h4>
                </div>
            
                <!-- Cuerpo modal --> 
                <div class="modal-body">
                    <p class="statusMsg"> </p>
                    <form role="form">
                        <div class="form-group">
                            <label for="inputName"> Nombre </label>
                            <input type="text" class="form-control" id="inputName" placeholder="Ingrese su nombre"/>
                        </div>
                    </form>
                </div>  
                <!-- Pie de página modal --> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm()"> Guardar </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Delete -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header content_message">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea eliminar el huésped?</b></span>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
          </div>
        </div>
      </div>
    </div>
    <script>
        $(document).on("click", ".open-modal-del", function () {
            var dataId_ = $(this).attr("data-id");
            document.getElementById("idselected").value = dataId_; 
        });
        var modalConfirm = function(callback){
          $(".btn_delete").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });
          
          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            $.ajax({
                url:"{{ route('deleteCategorie') }}",
                method:"POST",
                data:{_token:_token,id:id},
                success:function(data){
                    location.reload();
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });

        function submitContactForm(){
            var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            var name = $('#inputName').val();
            if(name.trim() == '' ){
                alert('Por favor ingrese el nombre de la categoría.');
                $('#inputName').focus();
                return false;
            }else{
                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type:'POST',
                    url:'{{ route("createcategoria") }}',
                    data:'contactFrmSubmit=1&name='+name,
                    beforeSend: function () {
                        $('.submitBtn').attr("disabled","disabled");
                        $('.modal-body').css('opacity', '.5');
                    },
                    success:function(msg){
                        location.reload();
                    }
                });
            }
        }
    </script>
@stop
