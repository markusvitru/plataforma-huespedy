@extends('layouts.platform')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDd_FwAluCVYrbSLKv49jGlgOuS3zy0UOk&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
@section('content')
    <div class="container_center">
        <div class="title_top">
            <a href="{{ URL::to('/admin/hotel') }}">
                <i class="fa fa-sign-in"></i>
            </a>
            <h1 class="text-center">Editar Hotel</h1>
        </div>
        {!! Form::model($hotel, [
            'method' => 'PATCH',
            'route' => ['hotel.update', $hotel->id],
            'enctype'=> 'multipart/from-data',
            'files'=> true,
            'name' => 'form_edit_client'
        ]) !!}

        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información del Hotel</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                    		<div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('name', 'Nombre Hotel:', ['class' => 'control-label']) !!}
                                    <input type="text" name="hname" id="hname" class="form-control input-lg" value="{{ $hotel->name }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('room_number', 'Agregar Cantidad de Cuartos:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="room_number" name="room_number" value="{{ $hotel->room_number }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('address', 'Dirección:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="address" name="address" value="{{ $hotel->address }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('country', 'País:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="country" name="country" value="{{ $hotel->country }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('city', 'Ciudad:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="city" name="city" value="{{ $hotel->city }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $hotel->email }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('phone', 'Teléfono:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $hotel->phone }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('check_in', 'Check In:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="check_in" name="check_in" value="{{ $hotel->check_in }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('check_out', 'Check Out:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="check_out" name="check_out" value="{{ $hotel->check_out }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('state', 'Estado:', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="state">
                                        <option value="" <?php if($hotel->state==''){ echo 'selected'; } ?>>-- Seleccione --</option>
                                        <option value="1" <?php if($hotel->state=='1'){ echo 'selected'; } ?>>Activo</option>
                                        <option value="0" <?php if($hotel->state=='0'){ echo 'selected'; } ?>>Suspendido</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                {!! Form::label('logo', 'Adjuntar Logo:', ['class' => 'control-label']) !!}
                                <div class="form-group">
                                    <?php
                                        if($hotel->url_logo!=''){
                                    ?>
                                        <div class="img-wrap">
                                            <span class="close">&times;</span>
                                            <img src="{{asset('images/logos/'.$hotel->url_logo)}}" class="img-fluid">
                                            <input type="hidden" name="url_logo" class="url_logo" value="{{ $hotel->url_logo }}">
                                            <input type="hidden" name="id_hotel" class="id_hotel" value="{{ $hotel->id }}">
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group">
                                                <input id="logo" class="form-control" name="logo" type="file">
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('gps', 'GPS:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="gps" name="gps" value="{{ $hotel->gps }}">
                                    <div class="pac-card" id="pac-card">
                                      <div>
                                        <div id="title">Buscador</div>
                                        <div id="type-selector" class="pac-controls">
                                          <input
                                            type="radio"
                                            name="type"
                                            id="changetype-all"
                                            checked="checked"
                                          />
                                          <label for="changetype-all">Todos</label>

                                          <input type="radio" name="type" id="changetype-establishment" />
                                          <label for="changetype-establishment">Establecimientos</label>

                                          <input type="radio" name="type" id="changetype-address" />
                                          <label for="changetype-address">Direcciones</label>

                                          <input type="radio" name="type" id="changetype-geocode" />
                                          <label for="changetype-geocode">Coordenadas</label>
                                        </div>
                                        <div id="strict-bounds-selector" class="pac-controls">
                                          <input type="checkbox" id="use-strict-bounds" value="" />
                                          <label for="use-strict-bounds">Límites estrictos</label>
                                        </div>
                                      </div>
                                      <div id="pac-container">
                                        <input id="pac-input" type="text" placeholder="Ingrese una ubicación" />
                                      </div>
                                    </div>
                                    <div id="map"></div>
                                    <div id="infowindow-content">
                                      <img src="" width="16" height="16" id="place-icon" />
                                      <span id="place-name" class="title"></span><br />
                                      <span id="place-address"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información del Contacto</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('contact_name', 'Nombre:', ['class' => 'control-label']) !!}
                                    <input type="text" name="contact_name" id="contact_name" class="form-control input-lg" value="{{ $hotel->contact_name }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('contact_email', 'Email:', ['class' => 'control-label']) !!}
                                    <input type="email" class="form-control" id="contact_email" name="contact_email" value="{{ $hotel->contact_email }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('contact_cell_phone', 'Celular:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="contact_cell_phone" name="contact_cell_phone" value="{{ $hotel->contact_cell_phone }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('contact_position', 'Cargo:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="contact_position" name="contact_position" value="{{ $hotel->contact_position }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información Administrador/es</h3>
                    </div>
                    <div class="card-body">
                        @if(!empty($admins))
                            @foreach($admins as $v)
                                <div class="row">
                                    <div class="col-12 col-sm-4">
                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                            <input type="text" name="nameadm1" id="nameadm1" class="form-control input-lg" value="{{ $v->name }}" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                        <div class="form-group">
                                            {!! Form::label('emailadmin', 'Email:', ['class' => 'control-label']) !!}
                                            <input type="email" class="form-control" id="emailadminadm1" name="emailadminadm1" value="{{ $v->email }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                        <div class="form-group">
                                            {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
                                            <input type="password" class="form-control" id="passwordadm1" name="passwordadm1">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="row justify-content-between">
                            <div>
                                <small class="text-danger text-center"><b>Si desea agregar un usuario que ya está registrado en nuestra plataforma por favor utilice el buscador, pero en caso de agregar un usuario no registrado de click en el botón "Agregar Usuario Nuevo"</b></small>
                            </div>
                            <div class="box col-12 col-sm-4 mt-4">
                                <span><b>Buscar Usuarios</b></span><br>
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Ingrese nombre de usuario" autocomplete="off"/>
                                    <div id="nameList">
                                    </div>
                                </div>
                                {{ csrf_field() }}
                            </div>
                            <div class="col-12 col-sm-3 text-center d-none"></div>
                            <div class="col-12 col-sm-4 mt-5 text-center">
                                <div class="btn btn-primary" id="no_register">Agregar Usuario Nuevo</div>
                            </div>
                        </div>
                        <div class="searched">
                            <h3>Usuario Registrado</h3>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                    <input type="text" name="namesearch" id="namesearch" class="form-control input-lg" required readonly/>
                                </div>
                            </div>
                        </div>
                        <div id="nuevo">
                            <h3>Nuevo Usuario 1</h3>
                            <div class="row duplicate">
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                        <input type="text" name="name1" id="name1" class="form-control input-lg"/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label('emailadmin', 'Email:', ['class' => 'control-label']) !!}
                                        <input type="email" class="form-control" id="emailadmin1" name="emailadmin1" onBlur="validarEmail('1');">
                                        <small class="text-danger ocult">El email ya esta registrado en el sistema por favor ingresa otro.</small>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
                                        <input type="password" class="form-control" id="password1" name="password1">
                                        <i class="fa fa-minus-circle" onclick="ocultaradm('+cantidad+')"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="show_item" id="dinamicos1">
                            </div>
                            <div class="col-12 text-center">
                                <div class="btn btn-primary" id="room_type" onclick="detectarEstado();">Agregar Otro Administrador
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Imágenes del hotel</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <?php
                                if($hotel->url_imgs==''){
                            ?>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('files', 'Cargar Imágenes:', ['class' => 'control-label']) !!}
                                        <input type="file" name="files[]" multiple >
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('files', 'Cargar Imágenes:', ['class' => 'control-label']) !!}
                                        <input type="file" name="files[]" multiple >
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12">
                                    <div class="row">
                                        <?php
                                            $imgs = explode(' / ', $hotel->url_imgs);
                                            $cantidadimgs = count($imgs);
                                            for($i=0;$i<$cantidadimgs;$i++){
                                        ?>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="img-wrap_">
                                                            <span onClick="sendNumber(<?php echo $i; ?>);" class="close_">&times;</span>
                                                            <img src="{{asset('images/hotels/'.$imgs[$i])}}" class="img-fluid">
                                                            <input type="hidden" name="urlimg<?php echo $i; ?>" id="urlimg<?php echo $i; ?>" value="{{ $imgs[$i] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php } ?>
                                        <input type="hidden" name="quantity" class="quantity" value="<?php echo $i-1; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center btn_save">
                <div class="justify-content-center">
                    <input type="hidden" name="id_h" class="id_h" value="{{ $hotel->id }}">
                    <input type="hidden" class="url_imgs" name="url_imgs" value="{{ $hotel->url_imgs }}">
                    <input type="hidden" name="numberimg" id="numberimg">
                    {!! Form::submit('Editar Hotel', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    <!-- Modal eliminar logo -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Realmente desea eliminar el logo?</h4>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal eliminar imágenes -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modalimg">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Realmente desea eliminar la imagen?</h4>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="modal-btn-si_">Si</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no_">No</button>
          </div>
        </div>
      </div>
    </div>
    <script>
        function sendNumber(number){
            document.getElementById("numberimg").value = number;
        }
        $(document).ready(function(){

            $('#name').keyup(function(){
                var query = $(this).val();
                if(query != '')
                {
                 var _token = $('input[name="_token"]').val();
                 $.ajax({
                  url:"{{ route('fetch') }}",
                  method:"POST",
                  data:{query:query, _token:_token},
                  success:function(data){
                    console.log(data);
                   $('#nameList').fadeIn();
                        $('#nameList').html(data);
                  }
                 });
                }
            });
            $(document).on('click', 'li', function(){
                $('.searched').css('display','block');
                $('#namesearch').val($(this).text());
                $('#nameList').fadeOut();
            });
            $('#no_register').on('click',function(){
                $('#nuevo').css('display','block');
            });

            var modalConfirm = function(callback){
              $(".img-wrap .close").on("click", function(){
                $("#mi-modal").modal('show');
              });
              $("#modal-btn-si").on("click", function(){
                callback(true);
                $("#mi-modal").modal('hide');
              });
              $("#modal-btn-no").on("click", function(){
                callback(false);
                $("#mi-modal").modal('hide');
              });
            };
            modalConfirm(function(confirm){
              if(confirm){
                //Acciones si el usuario confirma
                var query = $('.url_logo').val();
                if(query != ''){
                    var _token = $('input[name="_token"]').val();
                    var idhotel = $('.id_hotel').val();
                    $.ajax({
                        url:"{{ route('deleteimglogo') }}",
                        method:"POST",
                        data:{query:query,idhotel:idhotel,_token:_token},
                        success:function(data){
                            location.reload();
                        }
                    });
                }
              }else{
                //Acciones si el usuario no confirma
                $("#result").html("NO CONFIRMADO");
              }
            });

            var modalConfirm = function(callback){
              $(".img-wrap_ .close_").on("click", function(){
                $("#mi-modalimg").modal('show');
              });
              $("#modal-btn-si_").on("click", function(){
                callback(true);
                $("#mi-modalimg").modal('hide');
              });
              $("#modal-btn-no_").on("click", function(){
                callback(false);
                $("#mi-modalimg").modal('hide');
              });
            };
            modalConfirm(function(confirm){
              if(confirm){
                //Acciones si el usuario confirma
                var query = $('.url_imgs').val();
                if(query != ''){
                    var _token = $('input[name="_token"]').val();
                    var idh = $('.id_h').val();
                    var quantity = $('.quantity').val();
                    var number = $('#numberimg').val();
                    var url = 'urlimg'+number;
                    var delimg = 0;
                    var counter = 1;
                    var uimgs = '';
                    for(var i=0;i<=quantity;i++){
                        if(i==number){
                            delimg = document.getElementById(url).value;
                        }else{
                            var imgs = 'urlimg'+i;
                            var u = 'urlimg'+i;
                            if(counter==1){
                                u = document.getElementById(imgs).value;
                                uimgs = u;
                            }else{
                                u = document.getElementById(imgs).value;
                                uimgs = uimgs+" / "+u;
                            }
                            counter++;
                        }
                    }
                    $.ajax({
                        url:"{{ route('deleteImg') }}",
                        method:"POST",
                        data:{uimgs:uimgs,idh:idh,delimg:delimg,_token:_token},
                        success:function(data){
                            location.reload();
                        }
                    });
                }
              }else{
                //Acciones si el usuario no confirma
                $("#result").html("NO CONFIRMADO");
              }
            });

        });

        function validarEmail(v){
            var query = $('#emailadmin'+v).val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
              url:"{{ route('searchEmailUser') }}",
              method:"POST",
              data:{query:query, _token:_token},
              success:function(data){
                if(data.length > 0){
                    $('#emailadmin'+v).val('');
                    $('.ocult').css('display','block');
                }else{
                    $('.ocult').css('display','none');
                }
              }
            });
        }
        function detectarEstado(){
            var cantidad = $('.duplicate').length + 1;
            var c = $('.duplicate').length;
            $('#dinamicos'+c).after(
                '<div id="dinamicos'+cantidad+'"><h3>Nuevo Usuario '+cantidad+'</h3><div class="row duplicate"><div class="col-12 col-sm-4"><div class="form-group"><label>Nombre:</label><input type="text" name="name'+cantidad+'" id="name'+cantidad+'" class="form-control input-lg" required/></div></div><div class="col-12 col-sm-4"><div class="form-group"><label>Email:</label><input type="email" class="form-control" id="emailadmin'+cantidad+'" name="emailadmin'+cantidad+'" required></div></div><div class="col-12 col-sm-4"><div class="form-group"><label>Password:</label><input type="password" class="form-control" id="password'+cantidad+'" name="password'+cantidad+'" required><i class="fa fa-minus-circle" onclick="ocultar('+cantidad+')"></i></div></div></div></div>'
            );
        }
        function ocultar(obj){
            if(obj){
                $('#dinamicos'+obj).css('display','none');
            }
        }
        function ocultaradm(obj){
            $('#nuevo').css('display','none');
        }
    </script>
    <script>
      function initMap() {
          const map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: -33.8688, lng: 151.2195 },
            zoom: 13,
          });
          const card = document.getElementById("pac-card");
          const input = document.getElementById("pac-input");
          map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
          const autocomplete = new google.maps.places.Autocomplete(input);
          // Bind the map's bounds (viewport) property to the autocomplete object,
          // so that the autocomplete requests use the current map bounds for the
          // bounds option in the request.
          autocomplete.bindTo("bounds", map);
          // Set the data fields to return when the user selects a place.
          autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
          const infowindow = new google.maps.InfoWindow();
          const infowindowContent = document.getElementById("infowindow-content");
          infowindow.setContent(infowindowContent);
          const marker = new google.maps.Marker({
            map,
            anchorPoint: new google.maps.Point(0, -29),
          });
          autocomplete.addListener("place_changed", () => {
            infowindow.close();
            marker.setVisible(false);
            const place = autocomplete.getPlace();
            if (!place.geometry) {
              // User entered the name of a Place that was not suggested and
              // pressed the Enter key, or the Place Details request failed.
              window.alert("No details available for input: '" + place.name + "'");
              return;
            }
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
            } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17); // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            let address = "";

            if (place.address_components) {
              address = [
                (place.address_components[0] &&
                  place.address_components[0].short_name) ||
                  "",
                (place.address_components[1] &&
                  place.address_components[1].short_name) ||
                  "",
                (place.address_components[2] &&
                  place.address_components[2].short_name) ||
                  "",
              ].join(" ");
            }
            infowindowContent.children["place-icon"].src = place.icon;
            infowindowContent.children["place-name"].textContent = place.name;
            infowindowContent.children["place-address"].textContent = address;
            infowindow.open(map, marker);

            var coordenadas = '('+place.geometry.location.lat()+', '+place.geometry.location.lng()+')';
            $('#gps').val(coordenadas);
            map.addListener('click', function(mapsMouseEvent) {
              // Close the current InfoWindow.
              if(typeof infoWindow!== "undefined"){
                infoWindow.close();
              }
              // Create a new InfoWindow.
              infoWindow = new google.maps.InfoWindow({position: mapsMouseEvent.latLng});
              infoWindow.setContent(mapsMouseEvent.latLng.toString());
              infoWindow.open(map);
              $('#gps').val(mapsMouseEvent.latLng.toString());
            });
          });

          // Sets a listener on a radio button to change the filter type on Places
          // Autocomplete.
          function setupClickListener(id, types) {
            const radioButton = document.getElementById(id);
            radioButton.addEventListener("click", () => {
              autocomplete.setTypes(types);
            });
          }
          setupClickListener("changetype-all", []);
          setupClickListener("changetype-address", ["address"]);
          setupClickListener("changetype-establishment", ["establishment"]);
          setupClickListener("changetype-geocode", ["geocode"]);
          document
            .getElementById("use-strict-bounds")
            .addEventListener("click", function () {
              console.log("Checkbox clicked! New state=" + this.checked);
              autocomplete.setOptions({ strictBounds: this.checked });
            });
        }

    </script>
@endsection
