@extends('layouts.platform')
@section('content')
    <div class="row container_center">
        <div class="col-12 text-center">
            <div class="title_list">
                <h1>Listado de Hoteles</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="row content_center">
                <a href="{{ URL::to('/admin/hotel/create') }}" class="btn btn-primary">Agregar Hotel</a>
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Estado</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Teléfono</th>
                        <th class="text-center">Cuartos</th>
                        @role('super-admin')
                            <th class="col-xs-2 text-center">Acciones</th>
                        @endrole
                    </tr>
                </thead>
                <tbody>
                    @foreach ($hotel as $h)
                        <tr>
                            <td class="text-center">
                                @if($h->state==0)
                                    <i class="fa fa-eye text-danger" title="No Aprobado"></i>
                                @elseif($h->state=='ninguno')
                                    <i class="fa fa-question-circle" title="Ninguno"></i>
                                @else
                                    <i class="fa fa-check-circle text-success" title="Aprobado"></i>
                                @endif
                            </td>
                            <td>{{ $h->name }}</td>
                            <td>{{ $h->email }}</td>
                            <td>{{ $h->phone }}</td>
                            <td class="text-center">{{ $h->room_number }}</td>
                            <td class="text-center">
                                <a class="btn_menu open-modal" id="btn_menu" href="#my_modal" data-toggle="modal" data-id="<div class='row'><div class='col-12'><div class='form-group'><b>Nombre: </b><span>{{ $h->contact_name }}</span></div></div> <div class='col-12'><div class='form-group'><b>Email: </b><span>{{ $h->contact_email }}</span></div></div><div class='col-12'><div class='form-group'> <b>Celular: </b><span>{{ $h->contact_cell_phone }}</span></div></div><div class='col-12'><div class='form-group'><b>Cargo: </b><span>{{ $h->contact_position }}</span></div></div></div>">
                                    <i class="fa fa-user" title="Contacto"></i>
                                </a>
                                <a class="btn_menu" href="{{ URL::to('/admin/booking/sendhotel/'.$h->id) }}">
                                    <i class="fa fa-cog" title="Configuración Hotel"></i>
                                </a>
                                <a class="btn_menu" href="{{ url('/admin/hotel/'.$h->id.'/edit') }}">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                </a>
                                <a class="btn_menu btn_suspender open-modal-susp" href="#" data-id="{{ $h->id }}">
                                	<i class="fa fa-ban" title="Suspender"></i>
                                </a>
                                <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $h->id }}">
                                    <i class="fa fa-trash" title="Eliminar"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <input type="hidden" name="idselected" id="idselected">
        </div>
    </div>
    <!-- Modal Info Contact -->
    <div class="modal fade" id="ModalContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Información Contacto</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="modal-body">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Suspender -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header content_message">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea suspender el hotel?</b></span>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Delete -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal-del">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header content_message">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea eliminar el hotel?</b></span>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="modal-btn-si-del">Si</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no-del">No</button>
          </div>
        </div>
      </div>
    </div>
    <script>
        $(document).on("click", ".open-modal", function () {
            var dataId = $(this).attr("data-id");
            $("#modal-body").html(dataId);     
            $('#ModalContact').modal('show');
        });
        $(document).on("click", ".open-modal-susp", function () {
            var dataId_ = $(this).attr("data-id");
            document.getElementById("idselected").value = dataId_; 
        });
        var modalConfirm = function(callback){
          $(".btn_suspender").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });
          
          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            $.ajax({
                url:"{{ route('suspenderHotel') }}",
                method:"POST",
                data:{_token:_token,id:id},
                success:function(data){
                    location.reload();
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });

        $(document).on("click", ".open-modal-del", function () {
            var data_Id = $(this).attr("data-id");
            document.getElementById("idselected").value = data_Id; 
        });
        var modalConfirmDel = function(callback){
          $(".btn_delete").on("click", function(){
            $("#modal-del").modal('show');
          });

          $("#modal-btn-si-del").on("click", function(){
            callback(true);
            $("#modal-del").modal('hide');
          });
          
          $("#modal-btn-no-del").on("click", function(){
            callback(false);
            $("#modal-del").modal('hide');
          });
        };
        modalConfirmDel(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            $.ajax({
                url:"{{ route('deleteHotel') }}",
                method:"POST",
                data:{_token:_token,id:id},
                success:function(data){
                    location.reload();
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });
    </script>
@stop
