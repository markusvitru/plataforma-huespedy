<?php
	use App\User;
    use  App\Entities\Activities\Activity;
?>
@extends('layouts.platform')


@section('content')
<div class="container_center">
    <div class="title_top">
        <a href="{{ URL::to('/admin/activity') }}">
            <i class="fa fa-sign-in"></i>
        </a>
        <h1 class="text-center">Nueva Actividad</h1>
    </div>
    {!! Form::open([
		'route' => 'activity.store',
        'method' => 'post',
        'enctype'=> 'multipart/form-data'
	]) !!}
    <div class="row">
        <div class="col-12">
            <div class="card content_form">
                <div class="card-header">
                    <h3>Información de la Actividad</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                		<div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                <input type="text" name="name" id="name" class="form-control input-lg" required/>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="price" name="price">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('country', 'País:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="country" name="country">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('city', 'Ciudad:', ['class' => 'control-label']) !!}
                                <input type="text" class="form-control" id="city" name="city">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('initial_date', 'Fecha Inicial:', ['class' => 'control-label']) !!}
                                <input type="date" class="form-control" id="initial_date" name="initial_date">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('final_date', 'Fecha Final:', ['class' => 'control-label']) !!}
                                <input type="date" class="form-control" id="final_date" name="final_date">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                {!! Form::label('details', 'Detalles:', ['class' => 'control-label']) !!}
                                <textarea class="form-control" id="details" name="details"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card content_form">
                <div class="card-header">
                    <h3>Imágenes de la Actividad</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {!! Form::label('files', 'Cargar Imágenes:', ['class' => 'control-label']) !!}
                                <input type="file" name="files[]" multiple >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 text-center btn_save">
            <div class="justify-content-center">
                {!! Form::submit('Guardar Actividad', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop
