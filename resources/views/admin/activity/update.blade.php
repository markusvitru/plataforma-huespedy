@extends('layouts.platform')
@section('content')
    <div class="container_center">
        <div class="title_top">
            <a href="{{ URL::to('/admin/activity') }}">
                <i class="fa fa-sign-in"></i>
            </a>
            <h1 class="text-center">Editar Actividad</h1>
        </div>
        {!! Form::model($activity, [
            'method' => 'PATCH',
            'route' => ['activity.update', $activity->id],
            'enctype'=> 'multipart/from-data',
            'files'=> true,
            'name' => 'form_edit_client'
        ]) !!}
                    
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Información de la Actividad</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
                                    <input type="text" name="name" id="name" class="form-control input-lg" value="{{ $activity->name }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="price" name="price" value="{{ $activity->price }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('country', 'País:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="country" name="country" value="{{ $activity->country }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('city', 'Ciudad:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="city" name="city" value="{{ $activity->city }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('initial_date', 'Fecha Inicial:', ['class' => 'control-label']) !!}
                                    <input type="date" class="form-control" id="initial_date" name="initial_date" value="{{ $activity->initial_date }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('final_date', 'Fecha Final:', ['class' => 'control-label']) !!}
                                    <input type="date" class="form-control" id="final_date" name="final_date" value="{{ $activity->final_date }}">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    {!! Form::label('details', 'Detalles:', ['class' => 'control-label']) !!}
                                    <textarea class="form-control" id="details" name="details">{{ $activity->details }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">   
            <div class="col-12">
                <div class="card content_form">
                    <div class="card-header">
                        <h3>Imágenes de la Actividad</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <?php 
                                if($activity->url_imgs==''){
                            ?>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('files', 'Cargar Imágenes:', ['class' => 'control-label']) !!}
                                        <input type="file" name="files[]" multiple >
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('files', 'Cargar Imágenes:', ['class' => 'control-label']) !!}
                                        <input type="file" name="files[]" multiple >
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12">
                                    <div class="row">
                                        <?php
                                            $imgs = explode(' / ', $activity->url_imgs);
                                            $cantidadimgs = count($imgs);
                                            for($i=0;$i<$cantidadimgs;$i++){
                                        ?>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="img-wrap_">
                                                            <span onClick="sendNumber(<?php echo $i; ?>);" class="close_">&times;</span>
                                                            <img src="{{asset('images/activities/'.$imgs[$i])}}" class="img-fluid">
                                                            <input type="hidden" name="urlimg<?php echo $i; ?>" id="urlimg<?php echo $i; ?>" value="{{ $imgs[$i] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php } ?>
                                        <input type="hidden" name="quantity" class="quantity" value="<?php echo $i-1; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center btn_save">
                <div class="justify-content-center">
                    <input type="hidden" name="id_a" class="id_a" value="{{ $activity->id }}">
                    <input type="hidden" name="url_imgs" value="{{ $activity->url_imgs }}">
                    <input type="hidden" name="numberimg" id="numberimg">
                    {!! Form::submit('Editar Actividad', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- Modal eliminar imágenes -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modalimg">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Realmente desea eliminar la imagen?</h4>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="modal-btn-si_">Si</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no_">No</button>
          </div>
        </div>
      </div>
    </div>
    <script>
        function sendNumber(number){
            document.getElementById("numberimg").value = number;
        }
        $(document).ready(function(){
            var modalConfirm = function(callback){
              $(".img-wrap_ .close_").on("click", function(){
                $("#mi-modalimg").modal('show');
              });
              $("#modal-btn-si_").on("click", function(){
                callback(true);
                $("#mi-modalimg").modal('hide');
              });
              $("#modal-btn-no_").on("click", function(){
                callback(false);
                $("#mi-modalimg").modal('hide');
              });
            };
            modalConfirm(function(confirm){
              if(confirm){
                //Acciones si el usuario confirma
                var query = $('.url_imgs').val();
                if(query != ''){
                    var _token = $('input[name="_token"]').val();
                    var ida = $('.id_a').val();
                    var quantity = $('.quantity').val();
                    var number = $('#numberimg').val();
                    var url = 'urlimg'+number;
                    var delimg = 0;
                    var counter = 1;
                    var uimgs = '';
                    for(var i=0;i<=quantity;i++){
                        if(i==number){
                            delimg = document.getElementById(url).value;
                        }else{
                            var imgs = 'urlimg'+i;
                            var u = 'urlimg'+i;
                            if(counter==1){
                                u = document.getElementById(imgs).value;
                                uimgs = u;
                            }else{
                                u = document.getElementById(imgs).value;
                                uimgs = uimgs+" / "+u;
                            }
                            counter++;
                        }
                    }
                    $.ajax({
                        url:"{{ route('deleteImg') }}",
                        method:"POST",
                        data:{uimgs:uimgs,ida:ida,delimg:delimg,_token:_token},
                        success:function(data){
                            location.reload();
                        }
                    });
                }
              }else{
                //Acciones si el usuario no confirma
                $("#result").html("NO CONFIRMADO");
              }
            });
            
        });
    </script>
@endsection