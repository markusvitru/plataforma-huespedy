<?php
    use  App\Entities\Activities\Activity;
    use App\User;
?>
@extends('layouts.platform')

@section('content')
    <div class="row container_center">
        <div class="col-12 text-center">
            <div class="title_list">
                <h1>Listado de Actividades</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="row content_center">
                <a href="{{ URL::to('/admin/activity/create') }}" class="btn btn-primary">Agregar Actividad</a>
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Nombre</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Final</th>
                        <th>Precio</th>
                        @role('super-admin')
                            <th class="col-xs-2 text-center">Acciones</th>
                        @endrole
                    </tr>
                </thead>
                <tbody>
                    @foreach ($activity as $act)
                        <?php 
                            $mesl = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                            $dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado','Domingo');
                        ?>
                        <tr>
                            <td class="text-center">{{ $act->name }}</td>
                            <td>
                                <?php
                                    $in = explode("-", $act->initial_date);
                                    $dianaz=$in[2];
                                    $mesnaz=$in[1];
                                    $anonaz=$in[0];
                                    $nombredia= date('Y').'-'.$mesnaz.'-'.$dianaz;
                                    $f = $dias[date('N', strtotime($nombredia))].", ".$dianaz." de ".$mesl[$mesnaz-1];
                                ?>
                                {{ $f }}
                            </td>
                            <td>
                                <?php
                                    $in2 = explode("-", $act->final_date);
                                    $dianaz2=$in2[0];
                                    $mesnaz2=$in2[1];
                                    $anonaz2=$in2[0];
                                    $nombredia2= date('Y').'-'.$mesnaz2.'-'.$dianaz2;
                                    $f2 = $dias[date('N', strtotime($nombredia2))].", ".$dianaz2." de ".$mesl[$mesnaz2-1];
                                ?>
                                {{ $f2 }}
                            </td>
                            <td>{{ $act->price }}</td>
                            <td class="text-center">
                                <a class="btn_menu" href="{{ url('/admin/activity/'.$act->id.'/edit') }}">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                </a>
                                <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $act->id }}">
                                    <i class="fa fa-trash" title="Eliminar"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <input type="hidden" name="idselected" id="idselected">
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header content_message">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea eliminar la actividad?</b></span>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
            <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
          </div>
        </div>
      </div>
    </div>
    <script>
        $(document).on("click", ".open-modal-del", function () {
            var dataId_ = $(this).attr("data-id");
            document.getElementById("idselected").value = dataId_; 
        });
        var modalConfirm = function(callback){
          $(".btn_delete").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });
          
          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            $.ajax({
                url:"{{ route('deleteActivity') }}",
                method:"POST",
                data:{_token:_token,id:id},
                success:function(data){
                    location.reload();
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });
    </script>
@stop
