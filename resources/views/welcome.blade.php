<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Plataforma Huespedy</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #ffffff;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .welcome{
                background: #1b73b270;
                background: -moz-linear-gradient(left, #1b73b2 0%, #e30047 100%);
                background: -webkit-linear-gradient(left, #1b73b25c 0%,#e300472e 100%);
                background: linear-gradient(to right, #1b73b2d9 0%,#e30047bf 100%);
                color: #ffffff;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height welcome">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <!--<a href="{{ route('register') }}">Registro</a>-->
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Huespedy
                </div>

                <div class="links">
                    <a href="http://www.huespedy.com/planes/">Planes</a>
                    <a href="http://www.huespedy.com/category/hoteles-huespedy/">Hoteles</a>
                    <a href="http://www.huespedy.com/category/noticias-huespedy/">Noticias</a>
                </div>
            </div>
        </div>
    </body>
</html>
