<!DOCTYPE html>
<html>
<head>
	<title>Checkin</title>
</head>
<body>
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" style="font-family:Times">
		<tbody>
			<tr>
				<td style="background:rgb(255,255,255);padding:0px">
					<table border="0" cellpadding="0" cellspacing="0" align="center" valign="middle" style="margin:auto;max-width:730px">
						<tbody>
							<tr>
								<td style="max-width:730px"></td>
							</tr>
							<tr>
								<td align="left" style="width:730px">
									<a href="http://www.huespedy.com" target="_blank">
										<img src="http://intranet.huespedy.com/images/resources/huespedycabeceraemail.png" alt="Huespedy" align="left" style="display:block;width:730px;max-width:730px">
									</a>
								</td>
							</tr>
							<tr>
								<td colspan="2" bgcolor="#ffffff" style="border-radius:20px">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" valign="middle" style="width:657px">
										<tbody>
											<tr>
												<td align="left" face="Montserrat,Trebuchet MS,Lucida Grande" color="#222" style="padding:40px 20px;line-height:20px">
													<font face="Montserrat,Trebuchet MS,Lucida Grande" color="#222" style="font-size:18px;line-height:20px;font-weight:bold">
														¡Cordial Saludo!
													</font>
													<br><br>
													<font face="Trebuchet MS,Lucida Grande" color="#696969" style="font-size:14px;line-height:20px">
														Has realizado un checkout en nuestra app con la siguiente información.
														<br><br>
														<b>Huespedes:</b> {{ $name }}
														<br><br>
														<b>Ingreso:</b> {{ $arrival_date }}
														<br><br>
														<b>Salida:</b> {{ $departure_date }}
														<br><br>
														<b>Habitacion:</b> {{ $room }}
													</font>
													<br><br><br>
													<font face="Trebuchet MS,Lucida Grande" color="#696969" style="font-size:14px;line-height:20px">
														Atentamente
														<br>
														<span style="color:#144294">Equipo de soporte Huespedy</span>
													</font>
													<span style="font-size:14px"></span>
													<br><br>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr style="background:#144294;">
								<td colspan="2" style="width:730px">
									<center>
										<font face="Montserrat,Trebuchet MS,Lucida Grande" color="#222" style="font-size:18px;line-height:20px;font-weight:bold">
											<span style="color:#ffffff;padding-top:10px;display:block;">Síguenos en nuestras redes sociales</span>	
										</font>							
										<a href="https://www.facebook.com/Huespedy" style="display:inline-block;margin:22px 15px" target="_blank">
											<img src="http://intranet.huespedy.com/images/resources/logo-facebook.png" alt="Facebook Huespedy">
										</a>
										<a href="https://twitter.com/huespedy" style="display:inline-block;margin:22px 15px" target="_blank">
											<img src="http://www.sycgroup.co/wp-content/uploads/2019/01/twitter-logo.png" alt="Twitter Huespedy">
										</a>
										<a href="https://www.instagram.com/huespedy/" style="display:inline-block;margin:22px 15px" target="_blank">
											<img src="http://www.sycgroup.co/wp-content/uploads/2019/01/instagram-logo.png" alt="Instagram Huespedy">
										</a>&nbsp;
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>