<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNumberToOccupiedroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('occupied_rooms', function (Blueprint $table) {
            $table->string('number_of_adults')->nullable();
            $table->string('number_of_children')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('occupied_rooms', function (Blueprint $table) {
            $table->dropColumn('number_of_adults');
            $table->dropColumn('number_of_children');
        });
    }
}
