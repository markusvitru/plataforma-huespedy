<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOccupiedRoomToBookingCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_customers', function (Blueprint $table) {
            $table->integer('occupied_room_id')->nullable();
            $table->string('mobile_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_customers', function (Blueprint $table) {
            $table->dropColumn('occupied_room_id');
            $table->dropColumn('mobile_phone');
        });
    }
}
