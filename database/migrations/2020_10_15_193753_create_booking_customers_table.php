<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('booking_id');
            $table->foreign('booking_id')->references('id')->on('bookings');
            $table->string('type_identification');
            $table->string('identification_number');
            $table->string('full_name');
            $table->string('address');
            $table->string('country_id');
            $table->string('city_id');
            $table->string('foreign city')->nullable();
            $table->string('telephone')->nullable();
            $table->string('nacionality')->nullable();
            $table->string('genere_id')->nullable();
            $table->string('birthdate')->nullable();
            $table->string('blood_type')->nullable();
            $table->string('email');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_customers');
    }
}
