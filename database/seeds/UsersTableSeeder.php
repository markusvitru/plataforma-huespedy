<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User with role super-admin
        $admin = User::create([
        	'name' => 'super-admin',
        	'email' => 'ceo@huespedy.com',
        	'password' => bcrypt('12345')
        ]);
        $admin->assignRole('super-admin');

        //User with role hotel
        $hotel = User::create([
        	'name' => 'hotel',
        	'email' => 'markusvitru@gmail.com',
        	'password' => bcrypt('12345678')
        ]);
        $hotel->assignRole('admin-hotel');
    }
}
