<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //reset cache roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        //create permissions
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'read users']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'delete user']);
        
        Permission::create(['name' => 'create role']);
        Permission::create(['name' => 'read roles']);
        Permission::create(['name' => 'update role']);
        Permission::create(['name' => 'delete role']);

        Permission::create(['name' => 'create permission']);
        Permission::create(['name' => 'read permissions']);
        Permission::create(['name' => 'update permission']);
        Permission::create(['name' => 'delete permission']);

        Permission::create(['name' => 'create hotel']);
        Permission::create(['name' => 'read hotels']);
        Permission::create(['name' => 'update hotel']);
        Permission::create(['name' => 'delete hotel']);

        Permission::create(['name' => 'create activitie']);
        Permission::create(['name' => 'read activities']);
        Permission::create(['name' => 'update activitie']);
        Permission::create(['name' => 'delete activitie']);

        Permission::create(['name' => 'create booking']);
        Permission::create(['name' => 'read bookings']);
        Permission::create(['name' => 'update booking']);
        Permission::create(['name' => 'delete booking']);

        Permission::create(['name' => 'create categorie']);
        Permission::create(['name' => 'read categories']);
        Permission::create(['name' => 'update categorie']);
        Permission::create(['name' => 'delete categorie']);

        Permission::create(['name' => 'create consume_product']);
        Permission::create(['name' => 'read consume_products']);
        Permission::create(['name' => 'update consume_product']);
        Permission::create(['name' => 'delete consume_product']);
        
        Permission::create(['name' => 'create guest']);
        Permission::create(['name' => 'read guests']);
        Permission::create(['name' => 'update guest']);
        Permission::create(['name' => 'delete guest']);
        
        Permission::create(['name' => 'create product']);
        Permission::create(['name' => 'read products']);
        Permission::create(['name' => 'update product']);
        Permission::create(['name' => 'delete product']);

        //create roles and assign permissions
        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'admin-hotel']);
        $role->givePermissionTo('create hotel');
        $role->givePermissionTo('read hotels');
        $role->givePermissionTo('update hotel');
        $role->givePermissionTo('delete hotel');

        $role->givePermissionTo('create activitie');
        $role->givePermissionTo('read activities');
        $role->givePermissionTo('update activitie');
        $role->givePermissionTo('delete activitie');

        $role->givePermissionTo('create booking');
        $role->givePermissionTo('read bookings');
        $role->givePermissionTo('update booking');
        $role->givePermissionTo('delete booking');

        $role->givePermissionTo('create categorie');
        $role->givePermissionTo('read categories');
        $role->givePermissionTo('update categorie');
        $role->givePermissionTo('delete categorie');

        $role->givePermissionTo('create consume_product');
        $role->givePermissionTo('read consume_products');
        $role->givePermissionTo('update consume_product');
        $role->givePermissionTo('delete consume_product');

        $role->givePermissionTo('create guest');
        $role->givePermissionTo('read guests');
        $role->givePermissionTo('update guest');
        $role->givePermissionTo('delete guest');

        $role->givePermissionTo('create product');
        $role->givePermissionTo('read products');
        $role->givePermissionTo('update product');
        $role->givePermissionTo('delete product');
    }
}
