<?php

namespace App\Providers;

use App\Entities\Activities\Repositories\ActivityRepository;
use App\Entities\Activities\Repositories\Interfaces\ActivityRepositoryInterface;
use App\Entities\BookingCustomers\Repositories\BookingCustomerRepository;
use App\Entities\BookingCustomers\Repositories\Interfaces\BookingCustomerRepositoryInterface;
use App\Entities\Bookings\Repositories\BookingRepository;
use App\Entities\Bookings\Repositories\Interfaces\BookingRepositoryInterface;
use App\Entities\ConsumeProducts\Repositories\ConsumeProductRepository;
use App\Entities\ConsumeProducts\Repositories\Interfaces\ConsumeProductRepositoryInterface;
use App\Entities\Guests\Repositories\GuestRepository;
use App\Entities\Guests\Repositories\Interfaces\GuestRepositoryInterface;
use App\Entities\Hotels\Repositories\HotelRepository;
use App\Entities\Hotels\Repositories\Interfaces\HotelRepositoryInterface;
use App\Entities\OccupiedRooms\Repositories\Interfaces\OccupiedRoomRepositoryInterface;
use App\Entities\OccupiedRooms\Repositories\OccupiedRoomRepository;
use App\Entities\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Entities\Products\Repositories\ProductRepository;
use App\Entities\Rooms\Repositories\Interfaces\RoomRepositoryInterface;
use App\Entities\Rooms\Repositories\RoomRepository;
use App\Entities\RoomTypes\Repositories\Interfaces\RoomTypeRepositoryInterface;
use App\Entities\RoomTypes\Repositories\RoomTypeRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            ProductRepositoryInterface::class,
            ProductRepository::class
        );

        $this->app->bind(
            ActivityRepositoryInterface::class,
            ActivityRepository::class
        );

        $this->app->bind(
            BookingRepositoryInterface::class,
            BookingRepository::class
        );

        $this->app->bind(
            BookingCustomerRepositoryInterface::class,
            BookingCustomerRepository::class
        );

        $this->app->bind(
            HotelRepositoryInterface::class,
            HotelRepository::class
        );

        $this->app->bind(
            GuestRepositoryInterface::class,
            GuestRepository::class
        );

        $this->app->bind(
            RoomRepositoryInterface::class,
            RoomRepository::class
        );

        $this->app->bind(
            ConsumeProductRepositoryInterface::class,
            ConsumeProductRepository::class
        );

        $this->app->bind(
            OccupiedRoomRepositoryInterface::class,
            OccupiedRoomRepository::class
        );

        $this->app->bind(
            RoomTypeRepositoryInterface::class,
            RoomTypeRepository::class
        );
    }
}
