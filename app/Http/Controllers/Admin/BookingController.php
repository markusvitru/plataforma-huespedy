<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Entities\Bookings\Booking;
use App\Entities\Rooms\Room;
use App\Entities\Hotels\Hotel;
use App\Entities\OccupiedRooms\OccupiedRoom;
use App\Entities\RoomTypes\RoomType;
use App\Entities\BookingCustomers\Repositories\Interfaces\BookingCustomerRepositoryInterface;
use App\Entities\Bookings\Repositories\Interfaces\BookingRepositoryInterface;
use App\Entities\Bookings\Requests\CreateBookingRequest;
use App\Entities\Rooms\Repositories\Interfaces\RoomRepositoryInterface;

class BookingController extends Controller
{
    private $bookingInterface, $bookingCustomerInterface, $roomInterface;

    public function __construct(
        BookingRepositoryInterface $BookingRepositoryInterface,
        BookingCustomerRepositoryInterface $BookingCustomerRepositoryInterface,
        RoomRepositoryInterface $roomRepositoryInterface
    ) {
        $this->bookingInterface         = $BookingRepositoryInterface;
        $this->bookingCustomerInterface = $BookingCustomerRepositoryInterface;
        $this->roomInterface            = $roomRepositoryInterface;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendhotel($idhotel)
    {
        $user = Auth::user();
        $booking = Booking::select('*')->where('id_hotel', $idhotel)->where('state', '!=', '0')->get();
        $hotel = $idhotel;
        return view('admin.booking.list', compact('booking', 'user', 'hotel'));
    }
    public function cancelledreservation($idhotel)
    {
        $user = Auth::user();
        $booking = Booking::select('*')->where('id_hotel', $idhotel)->where('state', '0')->get();
        $hotel = $idhotel;
        $cancelada = 1;
        return view('admin.booking.list', compact('booking', 'user', 'hotel', 'cancelada'));
    }
    public function create()
    {
    }
    public function createwhithotel($hotel)
    {
        $h = $hotel;
        $roomtype = RoomType::select('*')->where('id_hotel', $h)->get();
        //namert
        return view('admin.booking.create', compact('h', 'roomtype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(CreateBookingRequest $request)
    {
        $booking = new Booking;
        //$bookingcustomer = new BookingCustomer;
        $booking->id_hotel = $request->id_hotel;
        $booking->id_room_type = $request->id_room_type;
        $booking->reservation_code = substr(md5(time()), 0, 6);
        $ident = '';
        $name = '';
        $email = '';
        $count = '';
        $city = '';
        $addre = '';
        $natio = '';
        $gende = '';
        $cellp = '';
        $birth = '';
        $blood = '';
        for ($i = 1; $i <= $request->persons; $i++) {
            $ide = 'identification' . $i;
            $nam = 'name' . $i;
            $ema = 'email' . $i;
            $cou = 'country' . $i;
            $cit = 'city' . $i;
            $add = 'address' . $i;
            $nat = 'nationality' . $i;
            $gen = 'gender' . $i;
            $cel = 'cellphone' . $i;
            $bir = 'birth' . $i;
            $blo = 'blood_type' . $i;
            if ($i == 1) {
                $ident = $request->$ide;
                $name = $request->$nam;
                $email = $request->$ema;
                $count = $request->$cou;
                $city = $request->$cit;
                $addre = $request->$add;
                $natio = $request->$nat;
                $gende = $request->$gen;
                $cellp = $request->$cel;
                $birth = $request->$bir;
                $blood = $request->$blo;
            } else {
                $ident = $ident . ' / ' . $request->ide;
                $name = $name . ' / ' . $request->$nam;
                $email = $email . ' / ' . $request->$ema;
                $count = $count . ' / ' . $request->$cou;
                $city = $city . ' / ' . $request->$cit;
                $addre = $addre . ' / ' . $request->$add;
                $natio = $natio . ' / ' . $request->$nat;
                $gende = $gende . ' / ' . $request->$gen;
                $cellp = $cellp . ' / ' . $request->$cel;
                $birth = $birth . ' / ' . $request->$bir;
                $blood = $blood . ' / ' . $request->$blo;
            }
        }
        $booking->identification = $ident;
        $booking->name = $name;
        $booking->email = $email;
        $booking->country = $count;
        $booking->city = $city;
        $booking->address = $addre;
        $booking->nationality = $natio;
        $booking->gender = $gende;
        $booking->cellphone = $cellp;
        $booking->birth = $birth;
        $booking->blood_type = $blood;
        $booking->persons = $request->persons;
        $booking->arrival_date = $request->arrival_date;
        $booking->departure_date = $request->departure_date;
        $booking->price = $request->price;
        $booking->hotel_notes = $request->hotel_notes;
        $booking->guest_notes = $request->guest_notes;
        $booking->state = 'precheckin';
        $hotel = Hotel::findOrFail($request->id_hotel);
        $emailsend = explode(' / ', $booking->email);
        $em = $emailsend[0];
        $namesend = explode(' / ', $booking->name);
        $na = $namesend[0];
        Mail::send('emails.booking', ['name' => $booking->name, 'code' => $booking->reservation_code, 'departure_date' => $request->departure_date, 'arrival_date' => $request->arrival_date, 'hotel' => $hotel->name, 'direccion' => $hotel->address], function ($mail) use ($em, $na) {
            $mail->to($em, $na)->subject($na . ' se acaba de realizar una reserva a su nombre.');
        });
        $booking->save();
        $occupiedroom = new OccupiedRoom;
        $occupiedroom->id_booking = $booking->id;
        $occupiedroom->id_room = $request->id_room;
        $occupiedroom->name = $request->room_name;
        $occupiedroom->save();
        $roomtype = RoomType::findOrFail($request->id_room_type);
        $nam = $roomtype->type;
        $cadenarecortada = str_replace($occupiedroom->name, "", $nam);
        $rt = $roomtype->availability;
        $total = $rt - 1;
        $roomtype->type = $cadenarecortada;
        $roomtype->availability = $total;
        $roomtype->save();
        return Redirect::to('admin/booking/sendhotel/' . $booking->id_hotel);
    }
    public function roomPrice(Request $request)
    {
        if ($request->get('query')) {
            $roomtype = Room::findOrFail($request->get('query'));
            return response()->json($roomtype->price);
        }
    }
    public function statistics()
    {
        $hotels = Hotel::all();
        return view('admin.booking.statistics', compact('hotels'));
    }
    public function consultDocument(Request $request)
    {
        if ($request->get('d')) {
            $booking = Booking::select('*')->where('identification', $request->get('d'))->get();
            return response()->json($booking);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function buscarRooms(Request $request)
    {
        if ($request->get('personas')) {
            $rooms = Room::select('*')->where('id_hotel', $request->get('idh'))->where('capacity', $request->get('personas'))->get();
            return response()->json($rooms);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = Booking::find($id);
        $roomtype = RoomType::select('*')->where('id_hotel', $booking->id_hotel)->get();
        $occupiedroom = OccupiedRoom::select('name')->where('id_booking', $booking->id)->get();
        return view('admin.booking.update', compact('booking', 'roomtype', 'occupiedroom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $booking = Booking::findOrFail($id);
        $booking->identification = $request->identification;
        $booking->name = $request->name;
        $booking->email = $request->email;
        $booking->country = $request->country;
        $booking->city = $request->city;
        $booking->address = $request->address;
        $booking->nationality = $request->nationality;
        $booking->gender = $request->gender;
        $booking->cellphone = $request->cellphone;
        $booking->birth = $request->birth;
        $booking->blood_type = $request->blood_type;
        $booking->arrival_date = $request->arrival_date;
        $booking->departure_date = $request->departure_date;
        $booking->persons = $request->persons;
        $booking->price = $request->price;
        $booking->hotel_notes = $request->hotel_notes;
        $booking->guest_notes = $request->guest_notes;
        $booking->save();
        return Redirect::to('admin/booking/sendhotel/' . $request->id_hotel);
    }
    public function saveReason(Request $request)
    {
        if ($request->get('rd')) {
            $reasontodelete = $request->get('rd');
            $id = $request->get('idb');
            $booking = Booking::findOrFail($id);
            $booking->reasontodelete = $reasontodelete;
            $booking->save();
        }
    }

    public function deleteBooking(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $booking = Booking::findOrFail($id);
            $booking->state = '0';
            $booking->save();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
