<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Entities\Bookings\Booking;
use App\Entities\Guests\Guest;
use App\Entities\Guests\Repositories\Interfaces\GuestRepositoryInterface;

class GuestController extends Controller
{
    private $guestInterface;

    public function __construct(GuestRepositoryInterface $guestRepositoryInterface)
    {
        $this->guestInterface = $guestRepositoryInterface;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendhotel($idhotel)
    {
        $booking = Booking::select('*')->where('id_hotel', $idhotel)->get();
        $hotel = $idhotel;
        return view('admin.guest.list', compact('booking', 'hotel'));
    }
    public function create()
    {
    }
    public function createwhithotel($hotel)
    {
        $user = Auth::user();
        $h = $hotel;
        return view('admin.guest.create', compact('user', 'h'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $guest = new Guest;
        $guest->id_hotel = $request->id_hotel;
        $guest->name = $request->name;
        $guest->identification = $request->identification;
        $guest->type_identification = $request->type_identification;
        $guest->email = $request->email;
        $guest->country = $request->country;
        $guest->city = $request->city;
        $guest->address = $request->address;
        $guest->nationality = $request->nationality;
        $guest->cellphone = $request->cellphone;
        $guest->birth = $request->birth;
        $guest->gender = $request->gender;
        $guest->blood_type = $request->blood_type;
        $guest->save();
        return Redirect::to('admin/guest/sendhotel/' . $guest->id_hotel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guest = Guest::find($id);
        return view('admin.guest.update', compact('guest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guest = Guest::findOrFail($id);
        $guest->name = $request->name;
        $guest->identification = $request->identification;
        $guest->type_identification = $request->type_identification;
        $guest->email = $request->email;
        $guest->country = $request->country;
        $guest->city = $request->city;
        $guest->address = $request->address;
        $guest->nationality = $request->nationality;
        $guest->cellphone = $request->cellphone;
        $guest->birth = $request->birth;
        $guest->gender = $request->gender;
        $guest->blood_type = $request->blood_type;

        $guest->save();
        return Redirect::to('admin/guest/sendhotel/' . $request->id_hotel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*$usuario = User::findOrFail($id);

        $usuario->removeRole($usuario->roles->implode('name', ', '));

        if($usuario->delete()){
            return redirect('/usuarios');
        }
        else{
            return response()->json([
                'mensaje' => 'Error al eliminar el usuario'
            ]);
        }*/
    }
}
