<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Activities\Activity;
use App\Entities\Activities\Repositories\Interfaces\ActivityRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ActivityController extends Controller
{
    private $activityInterface;

    public function __construct(ActivityRepositoryInterface $ActivityRepositoryInterface)
    {
        $this->activityInterface = $ActivityRepositoryInterface;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $activity = $this->activityInterface->listActivities();
        return view('admin.activity.list', compact('activity', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $activity = $this->activityInterface->listActivities();
        return view('admin.activity.create', compact('user', 'activity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->activityInterface->createActivity($request);
        return Redirect::to('admin/activity');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $activity = Activity::find($id);
        return view('admin.activity.update', compact('activity', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activity = Activity::findOrFail($id);
        $activity->name = $request->name;
        $activity->price = $request->price;
        $activity->country = $request->country;
        $activity->city = $request->city;
        $activity->initial_date = $request->initial_date;
        $activity->final_date = $request->final_date;
        $activity->details = $request->details;

        $imgs = '';
        // File upload configuration 
        $targetDir = base_path() . "/public/images/activities/";
        $allowTypes = array('jpg', 'png', 'jpeg', 'gif');
        $fileNames = array_filter($_FILES['files']['name']);
        if (!empty($fileNames)) {
            foreach ($_FILES['files']['name'] as $key => $val) {
                // File upload path 
                $fileName = basename($_FILES['files']['name'][$key]);
                $targetFilePath = $targetDir . $fileName;
                if ($key == 0) {
                    $imgs = $fileName;
                } else {
                    $imgs = $imgs . ' / ' . $fileName;
                }
                // Check whether file type is valid 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
                if (in_array($fileType, $allowTypes)) {
                    // Upload file to server 
                    move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath);
                } else {
                    echo 'Error al guardar imgs activities ';
                }
            }
        } else {
            echo 'Por favor seleccione un archivo para cargar.';
        }
        if ($imgs != '') {
            $activity->url_imgs = $imgs . ' / ' . $request->url_imgs;
        }

        $activity->save();
        return Redirect::to('admin/activity');
    }

    public function deleteImg(Request $request)
    {
        if ($request->get('delimg')) {
            $delimg = $request->get('delimg');
            $img = $delimg;
            $ruta = base_path() . '/public/images/activities/' . $img;
            unlink($ruta);
            $id = $request->get('ida');
            $uimgs = $request->get('uimgs');
            $activity = Activity::findOrFail($id);
            $activity->url_imgs = $uimgs;
            if ($activity->save()) {
                $resp = 'Logo eliminado correctamente';
            } else {
                $resp = 'El logo no pudo ser eliminado';
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteActivity(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $activity = Activity::findOrFail($id);
            $urlimgs = explode(' / ', $activity->url_imgs);
            $cantimgs = count($urlimgs);
            /*for ($i=0; $i <= $cantimgs; $i++) { 
                $ruta = base_path().'/public/images/activities/'.$urlimgs[$i];
                unlink($ruta);
            }*/
            $activity->delete();
        }
    }
    public function destroy($id)
    {
    }
}
