<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\User;
use App\Entities\Hotels\Hotel;
use App\Entities\Bookings\Booking;
use App\Entities\Hotels\Repositories\Interfaces\HotelRepositoryInterface;

class HotelController extends Controller
{
    private $hotelInterface;

    public function __construct(HotelRepositoryInterface $hotelRepositoryInterface)
    {
        $this->hotelInterface = $hotelRepositoryInterface;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (Auth::user()->id_hotel == 0) {
            $hotel = Hotel::select("*")->orderBy('id', 'desc')->get();
        } else {
            $hotel = Hotel::select("*")->where('id', Auth::user()->id_hotel)->orderBy('id', 'desc')->get();
        }

        return view('admin.hotel.list', compact('hotel', 'user'));
    }
    function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('users')
                ->where('name', 'LIKE', "%{$query}%")
                ->get();
            $output = '<div class="dropdown-menu" style="display:block; position:relative">';
            foreach ($data as $clave => $row) {
                $output .= '
               <li>' . $row->name . '</li>';
            }
            $output .= '</div>';
            echo $output;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $hotel = Hotel::select("*")->orderBy('id', 'desc')->first();
        return view('admin.hotel.create', compact('user', 'hotel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hotel = new Hotel;
        $hotel->name = $request->hname;
        $hotel->room_number = $request->room_number;
    	$hotel->address = $request->address;
    	$hotel->country = $request->country;
        $hotel->city = $request->city;
        $hotel->email = $request->email;
        $hotel->phone = $request->phone;
        $hotel->gps = $request->gps;
        $hotel->check_in = $request->check_in;
        $hotel->check_out = $request->check_out;
        $hotel->contact_name = $request->contact_name;
        $hotel->contact_email = $request->contact_email;
        $hotel->contact_cell_phone = $request->contact_cell_phone;
        $hotel->contact_position = $request->contact_position;
        $hotel->state = $request->state;
        $imgs = '';
        // File upload configuration
        $targetDir = base_path()."/public/images/hotels/";
        $allowTypes = array('jpg','png','jpeg','gif');
        $fileNames = array_filter($_FILES['files']['name']);
        if(!empty($fileNames)){
            foreach($_FILES['files']['name'] as $key=>$val){
                // File upload path
                $fileName = basename($_FILES['files']['name'][$key]);
                $targetFilePath = $targetDir . $fileName;
                if($key==0){
                    $imgs = $fileName;
                }else{
                    $imgs = $imgs.' / '.$fileName;
                }
                // Check whether file type is valid
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
                if(in_array($fileType, $allowTypes)){
                    // Upload file to server
                    move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath);
                }else{
                    echo 'Error al guardar imgs hotels ';
                }
            }
        }
        $hotel->url_imgs = $imgs;
        if($request->file('logo') != ""){
            $logo = $request->name.'.' .$request->file('logo')->guessExtension();
            $request->file('logo')->move(
                base_path().'/public/images/logos/', $logo
            );
            $hotel->url_logo = $logo;
        }
        $hotel->save();
        if($request->name1!=''){
            $conta = 1;
            if($request->name1 != ''){
                $conta;
            }if($request->name2 != ''){
                $conta=2;
            }if($request->name3 != ''){
                $conta=3;
            }if($request->name4 != ''){
                $conta=4;
            }if($request->name5 != ''){
                $conta=5;
            }if($request->name6 != ''){
                $conta=6;
            }if($request->name7 != ''){
                $conta=7;
            }if($request->name8 != ''){
                $conta=8;
            }if($request->name9 != ''){
                $conta=9;
            }if($request->name10 != ''){
                $conta=10;
            }if($request->name11 != ''){
                $conta=11;
            }if($request->name12 != ''){
                $conta=12;
            }
            $data = $request->all();
            $i=1;
            $j=1;
            $id_hotel = $hotel->id;
            for($i=1; $i<=$conta; $i++){
                $name = $data;
                $nam = 'name'.$i;
                $ema = 'emailadmin'.$i;
                $pas = 'password'.$i;
                $token = Str::random(60);
                $name['id_hotel'] = $id_hotel;
                $name['name'] = $request->$nam;
                $name['email'] = $request->$ema;
                $name['password'] = Hash::make($request->$pas);
                $name['access_token'] = $token;
                $name['api_token'] = hash('sha256', $token);
                User::create($name);
            }
        }
        /*if($request->namesearch!=''){
            $ns=$request->namesearch;

            $usersearched = User::select("id_hotel")->where('name', $ns)->first();
            $iduser = User::select("id")->where('name', $ns)->first();
            $user = User::findOrFail($iduser);
            echo $usersearched['id_hotel'];
            if($usersearched!=''){
                echo 'Entra 1';
                $user->id_hotel = $usersearched.'-'.$id_hotel ;
            }else{
                echo 'Entra 2';
                $user->id_hotel = $id_hotel;
            }
            $user->save();
        }*/
        return Redirect::to('admin/hotel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function searchEmailUser(Request $request)
    {
        if ($request->get('query')) {
            $email = $request->get('query');
            $user = User::select('email')->where('email', $email)->get();
            return response()->json($user);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $hotel = Hotel::find($id);
        $types = DB::select(DB::raw("SELECT * FROM room_types WHERE id_hotel='$id'"));
        $admins = User::select('*')->where('id_hotel', $id)->get();
        return view('admin.hotel.update', compact('hotel', 'user', 'types', 'admins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hotel = Hotel::findOrFail($id);
        $hotel->name = $request->hname;
        $hotel->room_number = $request->room_number;
        $hotel->address = $request->address;
        $hotel->country = $request->country;
        $hotel->city = $request->city;
        $hotel->email = $request->email;
        $hotel->phone = $request->phone;
        $hotel->gps = $request->gps;
        $hotel->check_in = $request->check_in;
        $hotel->check_out = $request->check_out;
        $hotel->contact_name = $request->contact_name;
        $hotel->contact_email = $request->contact_email;
        $hotel->contact_cell_phone = $request->contact_cell_phone;
        $hotel->contact_position = $request->contact_position;
        $hotel->state = $request->state;
        if ($request->file('logo') != "") {
            $logo = $request->name . '.' . $request->file('logo')->guessExtension();
            $request->file('logo')->move(
                base_path() . '/public/images/logos/',
                $logo
            );
            $hotel->url_logo = $logo;
        }
        $imgs = '';
        // File upload configuration
        $targetDir = base_path() . "/public/images/hotels/";
        $allowTypes = array('jpg', 'png', 'jpeg', 'gif');
        $fileNames = array_filter($_FILES['files']['name']);
        if (!empty($fileNames)) {
            foreach ($_FILES['files']['name'] as $key => $val) {
                // File upload path
                $fileName = basename($_FILES['files']['name'][$key]);
                $targetFilePath = $targetDir . $fileName;
                if ($key == 0) {
                    $imgs = $fileName;
                } else {
                    $imgs = $imgs . ' / ' . $fileName;
                }
                // Check whether file type is valid
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
                if (in_array($fileType, $allowTypes)) {
                    // Upload file to server
                    move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath);
                } else {
                    echo 'Error al guardar imgs hotels ';
                }
            }
        } else {
            echo 'Por favor seleccione un archivo para cargar.';
        }
        if ($imgs != '') {
            $hotel->url_imgs = $imgs . ' / ' . $request->url_imgs;
        }

        $hotel->save();
        if($request->name1!=''){
            $conta = 1;
            if ($request->name1 != '') {
                $conta;
            }
            if ($request->name2 != '') {
                $conta = 2;
            }
            if ($request->name3 != '') {
                $conta = 3;
            }
            if ($request->name4 != '') {
                $conta = 4;
            }
            if ($request->name5 != '') {
                $conta = 5;
            }
            if ($request->name6 != '') {
                $conta = 6;
            }
            if ($request->name7 != '') {
                $conta = 7;
            }
            if ($request->name8 != '') {
                $conta = 8;
            }
            if ($request->name9 != '') {
                $conta = 9;
            }
            if ($request->name10 != '') {
                $conta = 10;
            }
            if ($request->name11 != '') {
                $conta = 11;
            }
            if ($request->name12 != '') {
                $conta = 12;
            }
            $data = $request->all();
            $i = 1;
            $j = 1;
            $id_hotel = $hotel->id;
            for ($i = 1; $i <= $conta; $i++) {
                $name = $data;
                $nam = 'name' . $i;
                $ema = 'emailadmin' . $i;
                $pas = 'password' . $i;
                $token = Str::random(60);
                $name['id_hotel'] = $id_hotel;
                $name['name'] = $request->$nam;
                $name['email'] = $request->$ema;
                $name['password'] = Hash::make($request->$pas);
                $name['access_token'] = $token;
                $name['api_token'] = hash('sha256', $token);
                User::create($name);
            }
        }
        return Redirect::to('admin/hotel');
    }
    public function deleteImgLogo(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $logo = $query;
            $ruta = base_path() . '/public/images/logos/' . $logo;
            unlink($ruta);
            $id = $request->get('idhotel');
            $hotel = Hotel::findOrFail($id);
            $hotel->url_logo = '';
            if ($hotel->save()) {
                $resp = 'Logo eliminado correctamente';
            } else {
                $resp = 'El logo no pudo ser eliminado';
            }
        }
    }
    public function deleteImg(Request $request)
    {
        if ($request->get('delimg')) {
            $delimg = $request->get('delimg');
            $img = $delimg;
            $ruta = base_path() . '/public/images/hotels/' . $img;
            unlink($ruta);
            $id = $request->get('idh');
            $uimgs = $request->get('uimgs');
            $hotel = Hotel::findOrFail($id);
            $hotel->url_imgs = $uimgs;
            if ($hotel->save()) {
                $resp = 'Logo eliminado correctamente';
            } else {
                $resp = 'El logo no pudo ser eliminado';
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function suspenderHotel(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $hotel = Hotel::findOrFail($id);
            $hotel->state = '0';
            $hotel->save();
        }
    }
    public function deleteHotel(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $hotel = Hotel::findOrFail($id);
            $hotel->delete();
            $booking = Booking::select('*')->where('id_hotel', $id)->get();
            $booking->delete();
        }
    }
    public function destroy($id)
    {
    }
}
