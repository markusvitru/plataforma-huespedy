<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Rooms\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Entities\RoomTypes\RoomType;

class RoomTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }
    public function sendhotel($idhotel)
    {
        $user = Auth::user();
        $roomtype = RoomType::select('*')->where('id_hotel', $idhotel)->get();
        $hotel = $idhotel;
        return view('admin.roomtype.list', compact('roomtype', 'user', 'hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    public function createwhithotel($hotel)
    {
        $user = Auth::user();
        $h = $hotel;
        return view('admin.roomtype.create', compact('user', 'h'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conta = 1;
        if ($request->type1 != '') {
            $conta;
        }
        if ($request->type2 != '') {
            $conta = 2;
        }
        if ($request->type3 != '') {
            $conta = 3;
        }
        if ($request->type4 != '') {
            $conta = 4;
        }
        if ($request->type5 != '') {
            $conta = 5;
        }
        if ($request->type6 != '') {
            $conta = 6;
        }
        if ($request->type7 != '') {
            $conta = 7;
        }
        if ($request->type8 != '') {
            $conta = 8;
        }
        if ($request->type9 != '') {
            $conta = 9;
        }
        if ($request->type10 != '') {
            $conta = 10;
        }
        if ($request->type11 != '') {
            $conta = 11;
        }
        if ($request->type12 != '') {
            $conta = 12;
        }
        $data = $request->all();
        $i = 1;
        $j = 1;
        $id_hotel = $request->id_hotel;
        for ($i = 1; $i <= $conta; $i++) {
            $roomstype = $data;
            $t = 'type' . $i;
            $q = 'quantity' . $i;
            $c = 'capacity' . $i;
            $a = 'availability' . $i;
            $p = 'price' . $i;
            $o = 'observation' . $i;
            $roomstype['id_hotel'] = $id_hotel;
            //$roomstype['type'] = $request->$t;
            $roomstype['quantity'] = $request->$q;
            $roomstype['capacity'] = $request->$c;
            $roomstype['availability'] = $request->$a;
            $roomstype['price'] = $request->$p;
            $roomstype['observation'] = $request->$o;
            $r = RoomType::create($roomstype);
            $roomnum = explode('-', $request->$t);
            $array_num = count($roomnum);
            for ($j = 0; $j < $array_num; ++$j) {
                $room = new Room;
                $room->id_hotel = $id_hotel;
                $room->id_room_type = $r->id;
                $room->name = $roomnum[$j];
                $room->capacity = $r->capacity;
                $room->price = $r->price;
                $room->save();
            }
        }
        return Redirect::to('admin/roomtype/sendhotel/' . $id_hotel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roomtype = RoomType::find($id);
        return view('admin.roomtype.update', compact('roomtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $roomtype = RoomType::findOrFail($id);
        $roomtype->type = $request->type;
        $roomtype->capacity = $request->capacity;
        $roomtype->quantity = $request->quantity;
        $roomtype->availability = $request->availability;
        $roomtype->price = $request->price;
        $roomtype->observation = $request->observation;
        $roomtype->save();
        return Redirect::to('admin/roomtype/sendhotel/' . $request->id_hotel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
    public function deleteRoomType(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $roomtype = RoomType::findOrFail($id);
            $roomtype->delete();
        }
    }
}
