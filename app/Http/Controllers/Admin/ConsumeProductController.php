<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Entities\Products\Product;
use App\Entities\Bookings\Booking;
use App\Entities\ConsumeProducts\ConsumeProduct;

class ConsumeProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }
    public function sendhotel($idhotel)
    {
        $consumeproduct = ConsumeProduct::orderBy('id', 'desc')->get();
        $user = Auth::user();
        $hotel = $idhotel;
        return view('admin.consumeproduct.list', compact('consumeproduct', 'user', 'hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    public function createwhithotel($hotel)
    {
        $h = $hotel;
        $booking = Booking::select('*')->where('id_hotel', $h)->get();
        $product = Product::select('*')->where('id_hotel', $h)->get();
        return view('admin.consumeproduct.create', compact('h', 'booking', 'product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $consumeproduct = new ConsumeProduct;
        $conta = 1;
        if ($request->id_product1 != '') {
            $conta;
        }
        if ($request->id_product2 != '') {
            $conta = 2;
        }
        if ($request->id_product3 != '') {
            $conta = 3;
        }
        if ($request->id_product4 != '') {
            $conta = 4;
        }
        if ($request->id_product5 != '') {
            $conta = 5;
        }
        if ($request->id_product6 != '') {
            $conta = 6;
        }
        if ($request->id_product7 != '') {
            $conta = 7;
        }
        if ($request->id_product8 != '') {
            $conta = 8;
        }
        if ($request->id_product9 != '') {
            $conta = 9;
        }
        if ($request->id_product10 != '') {
            $conta = 10;
        }
        if ($request->id_product11 != '') {
            $conta = 11;
        }
        if ($request->id_product12 != '') {
            $conta = 12;
        }
        $data = $request->all();
        $i = 1;
        $j = 1;
        $idp = '';
        for ($i = 1; $i <= $conta; $i++) {
            $cp = $data;
            $ip = 'id_product' . $i;
            $idprod = $request->$ip;
            if ($i == 1) {
                $idp = $idprod;
            } else {
                $idp = $idprod . '-' . $idp;
            }
        }
        $consumeproduct->name = $request->name;
        $consumeproduct->name_booking = $request->name_booking;
        $consumeproduct->id_booking = $request->id_booking;
        $consumeproduct->id_products = $idp;
        $consumeproduct->total = $request->total;
        $consumeproduct->notes = $request->notes;
        $consumeproduct->state = $request->state;
        $consumeproduct->save();
        $id_hotel = $request->id_hotel;
        return Redirect::to('admin/consumeproduct/sendhotel/' . $id_hotel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function productPrice(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $idp = $query;
            $cprice = Product::findOrFail($idp);
            $price = $cprice->price;
            return response()->json($price);
        }
    }
    public function productPrice_(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $idp = $query;
            $cprice = Product::findOrFail($idp);
            $price = $cprice->price;
            return response()->json($price);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consumeproduct = ConsumeProduct::find($id);
        $b = Booking::find($consumeproduct->id_booking);
        $h = $b->id_hotel;
        $booking = Booking::select('*')->where('id_hotel', $h)->get();
        $product = Product::select('*')->where('id_hotel', $h)->get();
        return view('admin.consumeproduct.update', compact('consumeproduct', 'booking', 'product', 'h'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Redirect::to('admin/money');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*$usuario = User::findOrFail($id);

        $usuario->removeRole($usuario->roles->implode('name', ', '));

        if($usuario->delete()){
            return redirect('/usuarios');
        }
        else{
            return response()->json([
                'mensaje' => 'Error al eliminar el usuario'
            ]);
        }*/
    }
}
