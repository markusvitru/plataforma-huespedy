<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Entities\Products\Product;
use App\Entities\Products\Repositories\Interfaces\ProductRepositoryInterface;

class ProductController extends Controller
{
    private $productInterface;
    public function __construct(ProductRepositoryInterface $ProductRepositoryInterface)
    {
        $this->productInterface = $ProductRepositoryInterface;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }
    public function sendhotel($idhotel)
    {
        $user = Auth::user();
        $product = Product::where('id_hotel', $idhotel)->orderBy('id', 'desc')->get();
        $hotel = $idhotel;
        return view('admin.product.list', compact('product', 'user', 'hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*$user = Auth::user();
        $activity = Activity::select("*")->orderBy('id', 'desc')->first();
        return view('admin.activity.create', compact('user','activity'));*/
    }
    public function createwhithotel($hotel)
    {
        $user = Auth::user();
        $h = $hotel;
        return view('admin.product.create', compact('user', 'h'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product;
        $product->id_hotel = $request->id_hotel;
        $product->name = $request->name;
        $product->id_categoria = $request->id_categoria;
        $product->price = $request->price;
        $product->available_before_ckeckin = $request->available_before_ckeckin;
        $product->schedule = $request->schedule;
        $product->details = $request->details;
        if ($request->file('url_imgs') != "") {
            $image = $request->name . '.' . $request->file('url_imgs')->guessExtension();
            $request->file('url_imgs')->move(
                base_path() . '/public/images/products/',
                $image
            );
            $product->url_imgs = $image;
        }
        $product->save();
        return Redirect::to('admin/product/sendhotel/' . $product->id_hotel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('admin.product.update', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $product->id_categoria = $request->id_categoria;
        $product->price = $request->price;
        $product->available_before_ckeckin = $request->available_before_ckeckin;
        $product->schedule = $request->schedule;
        $product->details = $request->details;
        if ($request->file('url_imgs') != "") {
            $image = $request->name . '.' . $request->file('url_imgs')->guessExtension();
            $request->file('url_imgs')->move(
                base_path() . '/public/images/products/',
                $image
            );
            $product->url_imgs = $image;
        }
        $product->save();
        return Redirect::to('admin/product/sendhotel/' . $request->id_hotel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteImg(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $img = $query;
            $ruta = base_path() . '/public/images/products/' . $img;
            unlink($ruta);
            $id = $request->get('idproduct');
            $product = Product::findOrFail($id);
            $product->url_imgs = '';
            if ($product->save()) {
                $resp = 'Logo eliminado correctamente';
            } else {
                $resp = 'El logo no pudo ser eliminado';
            }
        }
    }
    public function destroy($id)
    {
    }
    public function deleteProduct(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $product = Product::findOrFail($id);
            $product->state = '0';
            $product->save();
        }
    }
}
