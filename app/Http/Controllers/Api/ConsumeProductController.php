<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Entities\ConsumeProducts\ConsumeProduct;
use App\Entities\OccupiedRooms\OccupiedRoom;
use App\Entities\Bookings\Booking;
use App\Entities\Hotels\Hotel;

class ConsumeProductController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	public function checkConsumption(Request $request)
	{

		$consume = ConsumeProduct::where('id_booking', $request->id)->get();
		if ($consume) {
			return response()->json([
				'status' => 0, 'data' => $consume
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este huesped no tiene consumos registrados en el sistema.'
			]);
		}
	}
	public function createConsumeProduct(Request $request)
	{
		$consume = new ConsumeProduct;
		$ids = "";
		$quantity = "";
		$notes = "";
		$suma = 0;
		foreach ($request->item as $key => $value) {

			$ids .= $value['id'] . "-";
			$quantity .= $value['quantity'] . "-";
			$suma += $value['price'] * $value['quantity'];
			$notes .= $value['notes'] . "-";
		}
		$ids = trim($ids, '-');
		$quantity = trim($quantity, '-');
		$notes = trim($notes, '-');
		$consume->id_booking = $request->idbooking;
		$consume->id_products = $ids;
		$consume->total = $suma;
		$consume->quantity = $quantity;
		$consume->notes = $notes;
		$consume->state = 'pendientes';
		$room = OccupiedRoom::where('id_booking', $request->idbooking)->first();
		$booking = Booking::where('id', $request->idbooking)->first();
		$hotel = Hotel::where('id', $request->idh)->first();
		if ($consume->save()) {
			Mail::send('emails.notifyorder', ['name' => $booking['name'], 'arrival_date' => $booking['arrival_date'], 'departure_date' => $booking['departure_date'], 'room' => $room->name, 'idproducts' => $ids, 'total' => $suma, 'quantity' => $quantity, 'notes' => $notes, 'namehotel' => $hotel['name']], function ($mail) use ($hotel) {
				$mail->to($hotel['email'], $hotel['name'])->subject($hotel['name'] . ' - Un usuario hizo checkin en el app.');
			});
			return response()->json([
				'status' => 0, 'data' => $consume
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este huesped no tiene consumos registrados en el sistema.'
			]);
		}
	}
}
