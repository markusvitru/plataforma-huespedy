<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Categories\Category;

class CategorieController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	public function listCategories(Request $request)
	{
		$categories = [];
		$categories = Category::whereIn('id', $request->categories)->get();

		if ($categories) {
			return response()->json([
				'status' => 0, 'data' => $categories
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este hotel no tiene categorias registradas en el sistema.'
			]);
		}
	}
}
