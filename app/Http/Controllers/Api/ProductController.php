<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Products\Product;

class ProductController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	public function listProducts(Request $request)
	{
		$products = [];
		$products = Product::whereIn('id', $request->products)->get();

		if ($products) {
			return response()->json([
				'status' => 0, 'data' => $products
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este hotel no tiene productos registrados en el sistema.'
			]);
		}
	}
	public function searchProducts(Request $request)
	{
		$idcategories = Product::where('id_hotel', $request->idh)->get();
		if ($idcategories) {
			return response()->json([
				'status' => 0, 'data' => $idcategories
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este hotel no tiene productos registrados en el sistema.'
			]);
		}
	}
	public function categoryProducts(Request $request)
	{
		$products = Product::where('id_categoria', $request->idc)->get();

		if ($products) {
			return response()->json([
				'status' => 0, 'data' => $products
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Esta categoria del hotel no tiene productos registrados en el sistema.'
			]);
		}
	}
}
