<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		$users = User::all();

		if (count($users) > 0) {
			return response()->json([
				'status' => 0, 'data' => $users
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'No se encontraron registros.'
			]);
		}
	}

	/**
	 * Store the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function store(Request $request)
	{
		return response()->json([
			'status' => 1, 'data' => Auth::user()
		]);
	}


	public function register()
	{

		$validator = Validator::make(request()->input(), [
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['error' => $validator->errors()], 401);
		}
		//
		request()->merge(['password' => bcrypt(request('password'))]);
		$user = User::create(request()->input());
		$success['token'] = $user
			->createToken('tasks api')
			->accessToken;


		return response()->json($success);
	}

	public function logout()
	{
		auth()->user()->tokens->each(function ($token, $key) {
			$token->delete();
		});
		return response()->json('Logged out successfully', 200);
	}

	public function login()
	{
		if (auth()->attempt(request()->input())) {
			$user = auth()->user();
			$success['token'] = $user
				->createToken('Passport Api')
				->accessToken;
			return response()->json($success, 200);
		} else {
			return response()->json(['error' => 'Unauthorized'], 401);
		}
	}
}
