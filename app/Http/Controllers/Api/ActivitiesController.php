<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  App\Entities\Activities\Activity;

class ActivitiesController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		$activities = Activity::all();

		if (count($activities) > 0) {
			return response()->json([
				'status' => 0, 'data' => $activities
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'No se encontraron registros.'
			]);
		}
	}

	/**
	 * Store the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function store(Request $request)
	{
		return response()->json([
			'status' => 1, 'data' => Auth::user()
		]);
	}
}
