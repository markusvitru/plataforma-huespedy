<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Hotels\Hotel;

class HotelController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}
	public function searchHotel(Request $request)
	{
		$hotel = Hotel::where('id', $request->id)->first();
		if ($hotel) {
			return response()->json([
				'status' => 0, 'data' => $hotel
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este hotel no se encuentra registrado en el sistema.', 'variable' => $request->id, 'otra' => 'Hola'
			]);
		}
	}
}
