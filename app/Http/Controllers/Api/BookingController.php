<?php

namespace App\Http\Controllers\Api;

use App\Entities\BookingCustomers\Repositories\Interfaces\BookingCustomerRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Entities\Bookings\Booking;
use App\Entities\Bookings\Repositories\Interfaces\BookingRepositoryInterface;
use App\Entities\ConsumeProducts\Repositories\Interfaces\ConsumeProductRepositoryInterface;
use App\Entities\OccupiedRooms\OccupiedRoom;
use App\Entities\Hotels\Hotel;
use App\Entities\Hotels\Repositories\Interfaces\HotelRepositoryInterface;
use App\Entities\OccupiedRooms\Repositories\Interfaces\OccupiedRoomRepositoryInterface;
use App\Entities\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Entities\Rooms\Repositories\Interfaces\RoomRepositoryInterface;
use App\Entities\RoomTypes\Repositories\Interfaces\RoomTypeRepositoryInterface;

class BookingController extends Controller
{
	private $bookingInterface, $bookingCustomerInterface, $occupiedRoomInterface;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(
		BookingRepositoryInterface $BookingRepositoryInterface,
		BookingCustomerRepositoryInterface $BookingCustomerRepositoryInterface,
		OccupiedRoomRepositoryInterface $occupiedRoomRepositoryInterface,
		RoomRepositoryInterface $roomRepositoryInterface,
		ConsumeProductRepositoryInterface $consumeProductRepositoryInterface,
		ProductRepositoryInterface $productRepositoryInterface,
		RoomTypeRepositoryInterface $roomTypeRepositoryInterface,
		HotelRepositoryInterface $hotelRepositoryInterface
	) {
		$this->bookingInterface 		= $BookingRepositoryInterface;
		$this->bookingCustomerInterface = $BookingCustomerRepositoryInterface;
		$this->occupiedRoomInterface 	= $occupiedRoomRepositoryInterface;
		$this->roomInterface			= $roomRepositoryInterface;
		$this->consumeProductInterface  = $consumeProductRepositoryInterface;
		$this->productInterface 	  	= $productRepositoryInterface;
		$this->roomTypeInterface 		= $roomTypeRepositoryInterface;
		$this->hotelInterface 			= $hotelRepositoryInterface;
	}
	public function checkout(Request $request)
	{
		$room 	 = OccupiedRoom::where('id_booking', $request->data['id'])->first();
		$booking = Booking::where('id', $request->data['id'])->first();
		$hotel 	 = Hotel::where('id', $booking['id_hotel'])->first();
		$booking->state = 'checkout';
		$booking->save();

		$emai   = explode(' / ', $booking['email']);
		$nam 	= explode(' / ', $booking['name']);

		Mail::send('emails.notifyreservation', ['name' => $booking['name'], 'arrival_date' => $booking['arrival_date'], 'departure_date' => $booking['departure_date'], 'room' => $room->name], function ($mail) use ($hotel) {
			$mail->to($hotel['email'], $hotel['name'])->subject($hotel['name'] . ' - Un usuario hizo checkout en el app.');
		});
		Mail::send('emails.checkout', ['name' => $request->data['name'], 'arrival_date' => $request->data['arrival_date'], 'departure_date' => $request->data['departure_date'], 'room' => $room->name], function ($mail) use ($room) {
			$mail->to('jhstyv@gmail.com', 'Steven')->subject('Huespedy - Un usuario hizo checkout en el app.');
		});
		Mail::send('emails.checkoutclient', ['name' => $request->data['name'], 'arrival_date' => $request->data['arrival_date'], 'departure_date' => $request->data['departure_date'], 'room' => $room->name], function ($mail) use ($emai, $nam) {
			$mail->to($emai[0], $nam[0])->subject('Huesped - Acabas de hacer checkout en nuestra app.');
		});
	}
	public function verifyId(Request $request)
	{
		$reservationid = Booking::where('id', $request->id)->first();
		if ($reservationid) {
			return response()->json([
				'status' => 0, 'data' => $reservationid
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este ID no se encuentra registrado en el sistema.', 'data' => $request
			]);
		}
	}
	public function verifyCode(Request $request)
	{
		$reservationcode = Booking::where('reservation_code', $request->reservationcode)->first();

		if ($reservationcode) {
			return response()->json([
				'status' => 0, 'data' => $reservationcode
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este codigo de reserva no se encuentra registrado en el sistema.'
			]);
		}
	}
	public function changeCheckin(Request $request)
	{
		$room 	 		= OccupiedRoom::where('id_booking', $request->id_booking['id'])->first();
		$booking 		= Booking::where('id', $request->id_booking['id'])->first();
		$hotel 	 	    = Hotel::where('id', $booking['id_hotel'])->first();
		$nam 	 		= $request->actuales['name'];
		$name 	 	    = $nam;
		$ide 			= $request->actuales['identification'];
		$identification = $ide;
		$ema 			= $request->actuales['email'];
		$email 			= $ema;
		$cou 			= $request->actuales['country'];
		$country 		= $cou;
		$cit 			= $request->actuales['city'];
		$city 			= $cit;
		$add 			= $request->actuales['address'];
		$address 		= $add;
		$nat 			= $request->actuales['nationality'];
		$nationality 	= $nat;
		$cel 			= $request->actuales['cellphone'];
		$cellphone 		= $cel;

		foreach ($request->nuevos['b'] as $value) {
			$name .= " / " . $value['name'];
			$identification .= " / " . $value['identification'];
			$email .= " / " . $value['email'];
			$country .= " / " . $value['country'];
			$city .= " / " . $value['city'];
			$address .= " / " . $value['address'];
			$nationality .= " / " . $value['nationality'];
			$cellphone .= " / " . $value['cellphone'];
		}

		if (!is_null($booking)) {
			$booking->state = 'checkin';
			$booking->name = $name;
			$booking->identification = $identification;
			$booking->email = $email;
			$booking->country = $country;
			$booking->city = $city;
			$booking->address = $address;
			$booking->nationality = $nationality;
			$booking->cellphone = $cellphone;
			$booking->save();

			$emai = explode(' / ', $booking['email']);
			$nam = explode(' / ', $booking['name']);

			Mail::send('emails.notifyreservation', ['name' => $booking['name'], 'arrival_date' => $booking['arrival_date'], 'departure_date' => $booking['departure_date'], 'room' => $room->name], function ($mail) use ($hotel) {
				$mail->to($hotel['email'], $hotel['name'])->subject($hotel['name'] . ' - Un usuario hizo checkin en el app.');
			});

			Mail::send('emails.notifyreservation', ['name' => $booking['name'], 'arrival_date' => $booking['arrival_date'], 'departure_date' => $booking['departure_date'], 'room' => $room->name], function ($mail) use ($booking) {
				$mail->to('jhstyv@gmail.com', 'Steven')->subject('Huespedy - Un usuario hizo checkin en el app.');
			});
			Mail::send('emails.reservationclient', ['name' => $booking['name'], 'arrival_date' => $booking['arrival_date'], 'departure_date' => $booking['departure_date'], 'room' => $room->name], function ($mail) use ($emai, $nam) {
				$mail->to($emai[0], $nam[0])->subject('Huesped - Acabas de hacer checkin en nuestra app.');
			});
			return response()->json([
				'status' => 0, 'data' => $booking
			]);
		} else {
			return response()->json([
				'status' => 1, 'message' => 'Este usuario no se encuentra registrado en el sistema.'
			]);
		}
	}

	public function storeBooking(Request $request)
	{
		$data =  $request->input('reserva');

		if (array_key_exists('token', $data) && !empty($data['token'])) {

			$hotel = $this->hotelInterface->findProductForName($data['empresaSesion']);

			if (empty($hotel)) {
				$response = ["response" => [
					"status" 	 => "ERROR",
					"codeStatus" => 400,
					"mesage" 	 => 'Error en registro de la reserva, No se ha encontrado el hotel'
				]];
				return response()->json($response);
			}

			$dataBooking = [
				'id_hotel' 		   	 => $hotel->id,
				'origin'   		   	 => $data['empresaSesion'],
				'reservation_code' 	 => substr(md5(time()), 0, 6),
				'arrival_date'     	 => $data['fechaInicio'],
				'number_of_adults' 	 => $data['cantidadAdulto'],
				'number_of_children' => $data['cantidadNinno'],
				'url_check_in'     	 => $data['urlCheckIn'],
				'departure_date'   	 => $data['fechaFin'],
				'persons'   	   	 => $data['cantidadAdulto'] + $data['cantidadNinno'],
				'price'   		   	 => $data['valor'],
				'hotel_notes'		 => $data['detalle'],
				'state' 		     => 'precheckin'
			];

			$booking = $this->bookingInterface->createBooking($dataBooking);

			$dataCustomer = [
				'booking_id'    	     => $booking->id,
				'type_identification'    => $data['cliente']['tipoIdentificacion'],
				'identification_number'  => $data['cliente']['documento'],
				'full_name'   			 => $data['cliente']['nombre'],
				'address'   	         => $data['cliente']['direccion'],
				'country_id'   		     => $data['cliente']['idPais'],
				'city_id'   		     => $data['cliente']['idMunicipio'],
				'foreign_city'   		 => $data['cliente']['ciudadExtranjera'],
				'telephone'   		 	 => $data['cliente']['telefono'],
				'email'   		 	     => $data['cliente']['email']
			];

			$this->bookingCustomerInterface->createBookingCustomer($dataCustomer);

			// $em = $bookingCustomer->email;
			// $na = $bookingCustomer->name;
			// Mail::send('emails.booking', ['name' => $bookingCustomer->name, 'code' => $booking->reservation_code, 'departure_date' => $booking->departure_date, 'arrival_date' => $booking->arrival_date, 'hotel' => $booking->hotel->name, 'direccion' => $booking->hotel->address], function ($mail) use ($em, $na) {
			// 	$mail->to($em, $na)->subject($na . ' se acaba de realizar una reserva a su nombre.');
			// });

			foreach ($data['habitaciones'] as $key => $room) {
				$findRoom = '';
				$findRoom = $this->roomInterface->findRoomForNameAndHotel($room['tipoHabitacion'], $hotel->id);
				if (empty($findRoom)) {
					$roomData = [
						'id_hotel' 		=> $hotel->id,
						'id_room_type' 	=> $room['idTipoHabitacion'],
						'name'  		=> $room['tipoHabitacion'],
						'price'   		=> $room['valorHabitacion'],
						'capacity'   	=> $room['cantidadAdultos'] + $room['cantidadNinno']
					];

					$findRoom = $this->roomInterface->createRoom($roomData);
				} else {
					$findRoom->capacity = $room['cantidadAdultos'] + $room['cantidadNinno'];
					$findRoom->price    = $room['valorHabitacion'];
					$findRoom->update();
				}

				$dataRoom = [
					'id_booking' 		 => $booking->id,
					'id_room'    		 => $findRoom->id,
					'name'  	 		 => $room['tipoHabitacion'],
					'number_of_adults'   => $room['cantidadAdultos'],
					'number_of_children' => $room['cantidadNinno']
				];
				$occupiedRoom[$key] =  $this->occupiedRoomInterface->createOccupiedRoom($dataRoom);
			}

			foreach ($data['servicios'] as $key => $service) {
				$findProduct = '';
				$findProduct = $this->productInterface->findProductForNameAndHotel($service['nombre'], $hotel->id);
				if (empty($findProduct)) {
					$productData = [
						'id_hotel' 	=> $hotel->id,
						'name'  	=> $service['nombre'],
						'price'   	=> $service['valor']
					];

					$findProduct = $this->productInterface->createProduct($productData);
				} else {

					$findProduct->price    = $service['valor'];
					$findProduct->update();
				}

				$dataService = [
					'name_booking' 	 => $data['empresaSesion'],
					'id_booking'     => $booking->id,
					'name'			 => $findProduct->name,
					'id_products'  	 => $findProduct->id,
					'total' 		 => $service['valor'],
					'quantity' 		 => $service['cantidad'],
					'state' 		 => 'pendientes'
				];

				$this->consumeProductInterface->createConsumeProduct($dataService);
			}

			$response = ["response" => [
				"status" 	 	   => "OK",
				"codeStatus" 	   => 200,
				"mesage" 	 	   => 'Registro exitoso',
				"booking_id" 	   => $booking->id,
				'reservation_code' => $booking->reservation_code,
				'arrival_date'     => $booking->arrival_date,
				'rooms'	     	   => $occupiedRoom
			]];

			return response()->json($response);
		}

		$response = ["response" => [
			"status" 	 => "ERROR",
			"codeStatus" => 400,
			"mesage" 	 => 'Error no se seguridad'
		]];
		return response()->json($response);
	}


	public function registerCheckin(Request $request)
	{
		$data =  $request->input('checkIn');

		if (array_key_exists('token', $data) && !empty($data['token'])) {

			$booking = $this->bookingInterface->findBookingForId($data['numeroReserva']);

			if (empty($booking)) {
				$response = ["response" => [
					"status" 	 => "ERROR",
					"codeStatus" => 400,
					"mesage" 	 => 'Error en registro de check-in: reserva inexistente'
				]];
				return response()->json($response);
			}


			foreach ($data['habitaciones'] as $key => $room) {
				$findRoom = '';
				$findRoom = $this->roomInterface->findRoomForNameAndHotel($room['tipoHabitacion'], $booking->id_hotel);
				if (empty($findRoom)) {
					$roomData = [
						'id_hotel' 		=> $booking->id_hotel,
						'id_room_type' 	=> $room['idTipoHabitacion'],
						'name'  		=> $room['tipoHabitacion'],
						'price'   		=> $room['valorHabitacion'],
						'capacity'   	=> $room['cantidadAdultos'] + $room['cantidadNinno']
					];

					$findRoom = $this->roomInterface->createRoom($roomData);
				} else {
					$findRoom->capacity = $room['cantidadAdultos'] + $room['cantidadNinno'];
					$findRoom->price    = $room['valorHabitacion'];
					$findRoom->update();
				}

				$dataRoom = [
					'id_booking' 		 => $booking->id,
					'id_room'    		 => $findRoom->id,
					'name'  	 		 => $room['tipoHabitacion'],
					'number_of_adults'   => $room['cantidadAdultos'],
					'number_of_children' => $room['cantidadNinno']
				];
				$occupiedRoom[$key] =  $this->occupiedRoomInterface->createOccupiedRoom($dataRoom);

				$dataResponse[$key] = [
					'idCheckInHabitacion' => $occupiedRoom[$key]->id,
					'huspedes' => []
				];

				foreach ($room['huespedes'] as $key2 => $customer) {
					$findCustomer = '';
					$findCustomer = $this->bookingCustomerInterface->findBookingCustomerForId($customer['documento'], $booking->id);
					if (empty($findCustomer)) {
						$dataCustomer = [
							'booking_id'    	     => $booking->id,
							'type_identification'    => $customer['tipoDocumento'],
							'identification_number'  => $customer['documento'],
							'full_name'   			 => $customer['nombre'] . ' ' . $customer['primerApellido'] . ' ' . $customer['segundoApellido'],
							'address'   	         => $customer['direccion'],
							'nacionality'   	     => $customer['nacionalidad'],
							'country_id'   		     => $customer['idPais'],
							'city_id'   		     => $customer['idCiudad'],
							'foreign_city'   		 => $customer['nombreCiudadExtranjera'],
							'telephone'   		 	 => $customer['telefonoFijo'],
							'mobile_phone' 		 	 => $customer['telefonoMovil'],
							'email'   		 	     => $customer['email'],
							'birthdate'   	 	     => $customer['fechaNacimiento'],
							'occupied_room_id'		 => $occupiedRoom[$key]->id
						];

						$findCustomer = $this->bookingCustomerInterface->createBookingCustomer($dataCustomer);
					} else {

						$findCustomer->type_identification 		= $customer['tipoDocumento'];
						$findCustomer->identification_number    = $customer['documento'];
						$findCustomer->full_name    		 	= $customer['nombre'] . ' ' . $customer['primerApellido'] . ' ' . $customer['segundoApellido'];
						$findCustomer->address    				= $customer['direccion'];
						$findCustomer->nacionality    			= $customer['nacionalidad'];
						$findCustomer->country_id    			= $customer['idPais'];
						$findCustomer->city_id    				= $customer['idCiudad'];
						$findCustomer->foreign_city    			= $customer['nombreCiudadExtranjera'];
						$findCustomer->telephone    			= $customer['telefonoFijo'];
						$findCustomer->mobile_phone    			= $customer['telefonoMovil'];
						$findCustomer->email    				= $customer['email'];
						$findCustomer->birthdate    			= $customer['fechaNacimiento'];
						$findCustomer->occupied_room_id    		= $occupiedRoom[$key]->id;
						$findCustomer->update();
					}
					$dataResponse[$key]['huspedes'][$key2] = [
						'idHuesped' => $findCustomer->id,
						'documento' => $findCustomer->identification_number
					];
				}
			}

			$booking->state = 'checkin';
			$booking->update();

			$productResponse = array();
			foreach ($booking->consumeProduct as $key3 => $products) {
				$productResponse[$key3] = [
					'id' 	 => $products->id,
					'nombre' => $products->name,
					'valor'  => $products->total
				];
			}

			$response = ["response" => [
				"status" 	 	   => "OK",
				"codeStatus" 	   => 200,
				"mesage" 	 	   => 'Registro de check-in: exitoso',
				"numeroCheckIn"    => $booking->id,
				"habitaciones"     => $dataResponse,
				"productos" 	   => $productResponse
			]];

			return response()->json($response);
		}

		$response = ["response" => [
			"status" 	 => "ERROR",
			"codeStatus" => 400,
			"mesage" 	 => 'Error no se seguridad'
		]];
		return response()->json($response);
	}
}
