<?php

namespace App\Entities\RoomTypes\Repositories;

use App\Entities\RoomTypes\RoomType;
use App\Entities\RoomTypes\Repositories\Interfaces\RoomTypeRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RoomTypeRepository implements RoomTypeRepositoryInterface
{
    private $columns = [
        'id_hotel',
        'type',
        'capacity',
        'quantity',
        'availability',
        'price',
        'observation'
    ];

    public function __construct(
        RoomType $roomType
    ) {
        $this->model = $roomType;
    }

    public function findRoomTypeForName($data): RoomType
    {
        try {
            return $this->model->where('id', $data)->first();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
