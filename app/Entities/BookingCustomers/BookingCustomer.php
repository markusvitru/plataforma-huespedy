<?php

namespace App\Entities\BookingCustomers;

use Illuminate\Database\Eloquent\Model;

class BookingCustomer extends Model
{
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id',
        'type_identification',
        'identification_number',
        'full_name',
        'address',
        'country_id',
        'city_id',
        'foreign_city',
        'telephone',
        'email',
        'occupied_room_id',
        'mobile_phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
