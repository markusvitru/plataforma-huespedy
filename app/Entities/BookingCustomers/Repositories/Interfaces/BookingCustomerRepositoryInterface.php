<?php

namespace App\Entities\BookingCustomers\Repositories\Interfaces;

use App\Entities\BookingCustomers\BookingCustomer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Support;

interface BookingCustomerRepositoryInterface
{
    public function createBookingCustomer($data): BookingCustomer;

    public function findBookingCustomerForId($id, $booking);
}
