<?php

namespace App\Entities\BookingCustomers\Repositories;

use App\Entities\BookingCustomers\BookingCustomer;
use App\Entities\BookingCustomers\Repositories\Interfaces\BookingCustomerRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;
use Illuminate\Database\Eloquent\Collection;

class BookingCustomerRepository implements BookingCustomerRepositoryInterface
{
    private $columns = [
        'booking_id',
        'type_identification',
        'identification_number',
        'full_name',
        'address',
        'country_id',
        'city_id',
        'foreign_city',
        'telephone',
        'email'
    ];

    public function __construct(
        BookingCustomer $bookingCustomer
    ) {
        $this->model = $bookingCustomer;
    }

    public function createBookingCustomer($data): BookingCustomer
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findBookingCustomerForId($id, $booking)
    {
        try {
            return $this->model->where('identification_number', $id)->where('booking_id', $booking)->first();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
