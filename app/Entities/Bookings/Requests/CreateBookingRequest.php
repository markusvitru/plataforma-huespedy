<?php

namespace App\Entities\Bookings\Requests;

use App\Entities\Base\BaseFormRequest;

class CreateBookingRequest extends BaseFormRequest
{

    public function rules()
    {
        return [
            'id_hotel'          => ['required'],
            'id_room_type'      => ['required'],
            'reservation_code'  => ['required']
        ];
    }
}
