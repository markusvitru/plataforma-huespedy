<?php

namespace App\Entities\Bookings\Repositories;

use App\Entities\Bookings\Booking;
use App\Entities\Bookings\Repositories\Interfaces\BookingRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;
use Illuminate\Database\Eloquent\Collection;

class BookingRepository implements BookingRepositoryInterface
{
    private $columns = [
        'name',
        'price',
        'country',
        'city',
        'initial_date',
        'final_date',
        'details',
        'url_imgs',
        'origen'

    ];

    public function __construct(
        Booking $booking
    ) {
        $this->model = $booking;
    }

    public function createBooking($data): Booking
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function findBookingForId($id)
    {
        try {
            return $this->model->find($id);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
