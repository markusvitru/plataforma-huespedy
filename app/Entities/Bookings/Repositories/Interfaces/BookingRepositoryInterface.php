<?php

namespace App\Entities\Bookings\Repositories\Interfaces;

use App\Entities\Bookings\Booking;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Support;

interface BookingRepositoryInterface
{
    public function createBooking($data): Booking;

    public function findBookingForId($data);
}
