<?php

namespace App\Entities\Bookings;

use App\Entities\BookingCustomers\BookingCustomer;
use App\Entities\ConsumeProducts\ConsumeProduct;
use App\Entities\Hotels\Hotel;
use App\Entities\OccupiedRooms\OccupiedRoom;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_hotel',
        'id_room_type',
        'reservation_code',
        'identification',
        'name',
        'email',
        'country',
        'city',
        'address',
        'nationality',
        'gender',
        'cellphone',
        'birth',
        'blood_type',
        'arrival_date',
        'departure_date',
        'persons',
        'price',
        'hotel_notes',
        'guest_notes',
        'state',
        'reasontodelete',
        'origin',
        'url_check_in',
        'number_of_children',
        'number_of_adults'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class, 'id_hotel');
    }

    public function customer()
    {
        return $this->hasMany(BookingCustomer::class);
    }

    public function occupiedRoom()
    {
        return $this->hasMany(OccupiedRoom::class);
    }

    public function consumeProduct()
    {
        return $this->hasMany(ConsumeProduct::class, 'id_booking');
    }
}
