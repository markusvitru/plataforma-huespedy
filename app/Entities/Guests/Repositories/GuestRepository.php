<?php

namespace App\Entities\Guests\Repositories;

use App\Entities\Guests\Guest;
use App\Entities\Guests\Repositories\Interfaces\GuestRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GuestRepository implements GuestRepositoryInterface
{
    private $columns = [
        'name',
        'price',
        'country',
        'city',
        'initial_date',
        'final_date',
        'details',
        'url_imgs'
    ];

    public function __construct(
        Guest $guest
    ) {
        $this->model = $guest;
    }
}
