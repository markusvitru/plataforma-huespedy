<?php

namespace App\Entities\Guests;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_hotel',
        'name',
        'identification',
        'type_identification',
        'email',
        'country',
        'city',
        'address',
        'nationality',
        'cellphone',
        'birth',
        'gender',
        'blood_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
