<?php

namespace App\Entities\Categories\Repositories;

use App\Entities\Categories\Category;
use App\Entities\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryRepository implements CategoryRepositoryInterface
{
    private $columns = [
        'name',
        'price',
        'country',
        'city',
        'initial_date',
        'final_date',
        'details',
        'url_imgs'
    ];

    public function __construct(
        Category $category
    ) {
        $this->model = $category;
    }
}
