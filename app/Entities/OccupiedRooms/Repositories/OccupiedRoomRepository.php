<?php

namespace App\Entities\OccupiedRooms\Repositories;

use App\Entities\OccupiedRooms\OccupiedRoom;
use App\Entities\OccupiedRooms\Repositories\Interfaces\OccupiedRoomRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OccupiedRoomRepository implements OccupiedRoomRepositoryInterface
{
    private $columns = [
        'id_booking',
        'id_room_type',
        'name'
    ];

    public function __construct(
        OccupiedRoom $occupiedRoom
    ) {
        $this->model = $occupiedRoom;
    }

    public function createOccupiedRoom($data): OccupiedRoom
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
