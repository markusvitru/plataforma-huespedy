<?php

namespace App\Entities\OccupiedRooms\Repositories\Interfaces;

use App\Entities\OccupiedRooms\OccupiedRoom;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Support;

interface OccupiedRoomRepositoryInterface
{
    public function createOccupiedRoom($data): OccupiedRoom;
}
