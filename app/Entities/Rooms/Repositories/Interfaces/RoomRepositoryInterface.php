<?php

namespace App\Entities\Rooms\Repositories\Interfaces;

use App\Entities\Rooms\Room;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Support;

interface RoomRepositoryInterface
{
    public function findRoomForNameAndHotel($room, $id);

    public function createRoom($data): Room;

    public function updateRoom($data);
}
