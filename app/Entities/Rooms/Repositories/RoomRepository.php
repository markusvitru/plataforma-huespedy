<?php

namespace App\Entities\Rooms\Repositories;

use App\Entities\Rooms\Room;
use App\Entities\Rooms\Repositories\Interfaces\RoomRepositoryInterface;
use Illuminate\Database\QueryException;

class RoomRepository implements RoomRepositoryInterface
{
    private $columns = [
        'id_hotel',
        'id_room_type',
        'name',
        'capacity',
        'price',
        'id'
    ];

    public function __construct(
        Room $room
    ) {
        $this->model = $room;
    }
    public function findRoomForNameAndHotel($room, $id)
    {
        try {
            return $this->model->where('name', $room)
                ->where('id_hotel', $id)
                ->first($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createRoom($data): Room
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            throw dd($e);
        }
    }

    public function updateRoom($data)
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw dd($e);
        }
    }
}
