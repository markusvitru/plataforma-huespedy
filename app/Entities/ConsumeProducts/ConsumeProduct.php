<?php

namespace App\Entities\ConsumeProducts;

use Illuminate\Database\Eloquent\Model;

class ConsumeProduct extends Model
{
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_booking',
        'id_booking',
        'id_products',
        'price',
        'total',
        'quantity',
        'notes',
        'state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
