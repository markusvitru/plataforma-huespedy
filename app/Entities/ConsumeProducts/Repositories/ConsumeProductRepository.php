<?php

namespace App\Entities\ConsumeProducts\Repositories;

use App\Entities\ConsumeProducts\ConsumeProduct;
use App\Entities\ConsumeProducts\Repositories\Interfaces\ConsumeProductRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ConsumeProductRepository implements ConsumeProductRepositoryInterface
{
    private $columns = [
        'name',
        'name_booking',
        'id_booking',
        'id_products',
        'price',
        'total',
        'quantity',
        'notes',
        'state'
    ];

    public function __construct(
        ConsumeProduct $consumeProduct
    ) {
        $this->model = $consumeProduct;
    }

    public function createConsumeProduct($data): ConsumeProduct
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
