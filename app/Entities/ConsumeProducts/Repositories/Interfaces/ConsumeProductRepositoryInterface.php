<?php

namespace App\Entities\ConsumeProducts\Repositories\Interfaces;

use App\Entities\ConsumeProducts\ConsumeProduct;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Support;

interface ConsumeProductRepositoryInterface
{
    public function createConsumeProduct($data): ConsumeProduct;
}
