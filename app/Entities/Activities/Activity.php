<?php

namespace  App\Entities\Activities;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'country',
        'city',
        'initial_date',
        'final_date',
        'details',
        'url_imgs'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
