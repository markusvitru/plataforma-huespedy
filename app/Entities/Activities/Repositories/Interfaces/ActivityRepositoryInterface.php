<?php

namespace App\Entities\Activities\Repositories\Interfaces;

use App\Entities\Activities\Activity;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

interface ActivityRepositoryInterface
{
    public function listActivities(): Collection;

    public function createActivity($data): Activity;

    public function saveCoverImage(UploadedFile $file): string;
}
