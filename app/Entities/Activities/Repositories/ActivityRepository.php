<?php

namespace App\Entities\Activities\Repositories;

use App\Entities\Activities\Activity;
use App\Entities\Activities\Repositories\Interfaces\ActivityRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;


class ActivityRepository implements ActivityRepositoryInterface
{
    private $columns = [
        'name',
        'price',
        'country',
        'city',
        'initial_date',
        'final_date',
        'details',
        'url_imgs'
    ];

    public function __construct(
        Activity $activity
    ) {
        $this->model = $activity;
    }

    public function listActivities(): Collection
    {
        try {
            return  $this->model->orderBy('id', 'desc')->get();
        } catch (QueryException $e) {
            throw dd($e);
        }
    }

    public function saveCoverImage(UploadedFile $file): string
    {
        return $file->store('activities', ['disk' => 'public']);
    }

    public function createActivity($data): Activity
    {
        $activity = $data->input();
        $imgs = '';
        $targetDir  = base_path() . "/public/images/activities/";
        $allowTypes = array('jpg', 'png', 'jpeg', 'gif');
        $fileNames  = array_filter($_FILES['files']['name']);
        if (!empty($fileNames)) {
            foreach ($_FILES['files']['name'] as $key => $val) {
                // File upload path 
                $fileName = basename($_FILES['files']['name'][$key]);
                $targetFilePath = $targetDir . $fileName;
                if ($key == 0) {
                    $imgs = $fileName;
                } else {
                    $imgs = $imgs . ' / ' . $fileName;
                }
                // Check whether file type is valid 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
                if (in_array($fileType, $allowTypes)) {
                    // Upload file to server 
                    move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath);
                } else {
                    echo 'Error al guardar imgs activities ';
                }
            }
        } else {
            echo 'Por favor seleccione un archivo para cargar.';
        }
        $activity['url_imgs'] = $imgs;

        try {
            return $this->model->create($activity);
        } catch (QueryException $e) {
            throw dd($e);
        }
    }
}
