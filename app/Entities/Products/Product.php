<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_hotel',
        'id_categoria',
        'name',
        'price',
        'schedule',
        'details',
        'available_before_ckeckin',
        'url_imgs',
        'state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
