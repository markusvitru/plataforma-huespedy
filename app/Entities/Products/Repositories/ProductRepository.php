<?php

namespace App\Entities\Products\Repositories;

use App\Entities\Products\Product;
use App\Entities\Products\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Database\QueryException;

class ProductRepository implements ProductRepositoryInterface
{
    private $columns = [
        'id',
        'id_hotel',
        'id_categoria',
        'name',
        'price',
        'schedule',
        'details',
        'available_before_ckeckin',
        'url_imgs',
        'state'
    ];

    public function __construct(
        Product $product
    ) {
        $this->model = $product;
    }

    public function findProductForNameAndHotel($data, $id)
    {
        try {
            return $this->model->where('name', 'like', '%' . $data . '%')
                ->where('id_hotel', $id)
                ->first($this->columns);
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }

    public function createProduct($data): Product
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            throw dd($e);
        }
    }
}
