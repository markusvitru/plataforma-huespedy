<?php

namespace App\Entities\Products\Repositories\Interfaces;

use App\Entities\Products\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Support;

interface ProductRepositoryInterface
{
    public function findProductForNameAndHotel($data, $id);

    public function createProduct($data): Product;
}
