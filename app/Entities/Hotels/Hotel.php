<?php

namespace App\Entities\Hotels;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'room_number',
        'address',
        'country',
        'city',
        'email',
        'phone',
        'gps',
        'check_in',
        'check_out',
        'contact_name',
        'contact_email',
        'contact_cell_phone',
        'contact_position',
        'id_user',
        'url_imgs',
        'url_logo',
        'state',
        'web_site',
        'redes',
        'trip_advisor',
        'booking',
        'airbnb'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
