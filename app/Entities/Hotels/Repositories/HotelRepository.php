<?php

namespace App\Entities\Hotels\Repositories;

use App\Entities\Hotels\Hotel;
use App\Entities\Hotels\Repositories\Interfaces\HotelRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HotelRepository implements HotelRepositoryInterface
{
    private $columns = [];

    public function __construct(
        Hotel $hotel
    ) {
        $this->model = $hotel;
    }

    public function findProductForName($data)
    {
        try {
            return $this->model->where('name', 'like', '%' . $data . '%')
                ->first();
        } catch (QueryException $e) {
            abort(503, $e->getMessage());
        }
    }
}
