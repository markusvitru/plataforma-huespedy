<?php

namespace App\Entities\Hotels\Repositories\Interfaces;

use App\Entities\Hotels\Hotel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Support;

interface HotelRepositoryInterface
{
    public function findProductForName($data);
}
